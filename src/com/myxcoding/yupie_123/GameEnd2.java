package com.myxcoding.yupie_123;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameEnd2 extends BukkitRunnable {

	private YuPball plugin;

	public GameEnd2(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		for (final Player p : plugin.Arena2) {
			p.sendMessage(plugin.message + ChatColor.GREEN + "Game Ended!");
			p.getInventory().clear();
			plugin.InGame2 = false;
			Bukkit.getServer().getScheduler()
					.scheduleSyncDelayedTask(plugin, new Runnable() {
						@Override
						public void run() {
							double X = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.X");
							double Y = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Y");
							double Z = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Z");
							float Yaw = plugin.getConfig().getInt(
									"Arena.Arena 2.Lobby.Yaw");
							float Pitch = plugin.getConfig().getInt(
									"Arena.Arena 2.Lobby.Pitch");
							String world = plugin.getConfig().getString(
									"Arena.Arena 2.Lobby.World");
							Location location = new Location(plugin.getServer()
									.getWorld(world), X, Y, Z, Yaw, Pitch);
							p.teleport(location);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "Taking you back to the Lobby!");
						}

					}, (20 * 30));
		}
	}

}
