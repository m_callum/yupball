package com.myxcoding.yupie_123.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.myxcoding.yupie_123.YuPball;

public class InventoryClick implements Listener {

	private YuPball plugin;

	public InventoryClick(YuPball plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		try {
			if (event.getInventory().getName().equalsIgnoreCase("shop")) {
				Player player = (Player) event.getWhoClicked();
				// 10 Snowballs
				if (event.getCurrentItem().getType() == Material.IRON_INGOT) {
					event.setCancelled(true);
					event.setResult(Result.DENY);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 50) {
						player.getInventory().addItem(
								new ItemStack(Material.SNOW_BALL, 10));
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 50;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// 50 Snowballs
				} else if (event.getCurrentItem().getType() == Material.GOLD_INGOT) {
					event.setCancelled(true);
					event.setResult(Result.DENY);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 200) {
						player.getInventory().addItem(
								new ItemStack(Material.SNOW_BALL, 50));
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 200;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// 100 Snowballs
				} else if (event.getCurrentItem().getType() == Material.DIAMOND) {
					event.setCancelled(true);
					event.setResult(Result.DENY);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 300) {
						player.getInventory().addItem(
								new ItemStack(Material.SNOW_BALL, 100));
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 300;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// Bomb
				} else if (event.getCurrentItem().getType() == Material.EGG) {
					event.setCancelled(true);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 200) {
						ItemStack bomb = new ItemStack(Material.EGG, 1);
						ItemMeta bM = bomb.getItemMeta();
						bM.setDisplayName(ChatColor.AQUA + "Bomb");
						List<String> bS = new ArrayList<String>();
						bS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Right click to throw!");
						bM.setLore(bS);
						bomb.setItemMeta(bM);
						player.getInventory().addItem(bomb);
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 200;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// Speed
				} else if (event.getCurrentItem().getType() == Material.FEATHER) {
					event.setCancelled(true);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 200) {
						ItemStack bomb = new ItemStack(Material.FEATHER, 1);
						ItemMeta bM = bomb.getItemMeta();
						bM.setDisplayName(ChatColor.AQUA + "Speed (2:00)");
						List<String> bS = new ArrayList<String>();
						bS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Right click to consume!");
						bM.setLore(bS);
						bomb.setItemMeta(bM);
						player.getInventory().addItem(bomb);
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 200;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// Jump
				} else if (event.getCurrentItem().getType() == Material.STRING) {
					event.setCancelled(true);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 300) {
						ItemStack bomb = new ItemStack(Material.STRING, 1);
						ItemMeta bM = bomb.getItemMeta();
						bM.setDisplayName(ChatColor.AQUA + "Jump (2:00)");
						List<String> bS = new ArrayList<String>();
						bS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Right click to consume!");
						bM.setLore(bS);
						bomb.setItemMeta(bM);
						player.getInventory().addItem(bomb);
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 300;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// Rocket Launcher
				} else if (event.getCurrentItem().getType() == Material.STICK) {
					event.setCancelled(true);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 500) {
						ItemStack bomb = new ItemStack(Material.STICK, 1);
						ItemMeta bM = bomb.getItemMeta();
						bM.setDisplayName(ChatColor.AQUA + "Rocket Launcher");
						List<String> bS = new ArrayList<String>();
						bS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Right click to use!");
						bM.setLore(bS);
						bomb.setItemMeta(bM);
						player.getInventory().addItem(bomb);
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 500;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
					// Mine
				} else if (event.getCurrentItem().getType() == Material.STONE_PLATE) {
					event.setCancelled(true);
					if (plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points") >= 400) {
						ItemStack bomb = new ItemStack(Material.STONE_PLATE, 1);
						ItemMeta bM = bomb.getItemMeta();
						bM.setDisplayName(ChatColor.AQUA + "Mine");
						List<String> bS = new ArrayList<String>();
						bS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Place on floor to use!");
						bM.setLore(bS);
						bomb.setItemMeta(bM);
						player.getInventory().addItem(bomb);
						int afterPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points") - 400;
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Points",
								afterPoints);
					} else {
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You do not have enough points to buy that!");
					}
				} else {
					player.sendMessage(plugin.message + ChatColor.GOLD
							+ "Please click an item to buy!");
				}
			}
			if (event.getSlotType() == InventoryType.SlotType.ARMOR) {
				event.setCancelled(true);
			}
			if (event.getInventory().getType() == InventoryType.CRAFTING) {
				if (event.getCurrentItem().getType() == Material.EMERALD) {
					Player player = (Player) event.getWhoClicked();
					event.setCancelled(true);
					Bukkit.dispatchCommand(player, "shop");
					int points = plugin.getStats().getInt(
							"Data.Stats." + player.getName() + ".Points");
					player.sendMessage(plugin.message + ChatColor.GREEN
							+ "You have " + ChatColor.BLUE + points
							+ ChatColor.GREEN + " points!");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
