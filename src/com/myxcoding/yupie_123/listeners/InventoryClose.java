package com.myxcoding.yupie_123.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryClose implements Listener {

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if(event.getInventory().getName().equalsIgnoreCase("shop")) {
			event.getPlayer().setNoDamageTicks(0);
		}
	}
	
}
