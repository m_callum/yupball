package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.myxcoding.yupie_123.YuPball;

public class Site implements CommandExecutor {

	private YuPball plugin;

	public Site(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

			String site = plugin.getConfig().getString("Site");
			sender.sendMessage(ChatColor.GOLD + "Site: " + ChatColor.AQUA
					+ site);

		return false;
	}

}
