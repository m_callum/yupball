package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitTask;

import com.myxcoding.yupie_123.GameEnd1;
import com.myxcoding.yupie_123.GameEnd2;
import com.myxcoding.yupie_123.YuPball;

public class ForceEnd implements CommandExecutor {

	private YuPball plugin;

	public ForceEnd(YuPball plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean onCommand(CommandSender player, Command command,
			String label, String[] args) {
		if (player.hasPermission("yupball.forceend")) {
			if (args.length > 1 || args.length < 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /forceend <1/2>");
			} else {
				if (args[0].equalsIgnoreCase("1")) {
					if (plugin.InGame1 == true) {
						plugin.Goals1red = 0;
						plugin.Goals1blue = 0;
						BukkitTask GameStart1 = new GameEnd1(plugin)
								.runTaskTimer(plugin, 0, -1);
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You ended the game in Arena 1!");
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That game is not currently in progress!");
					}
				} else if (args[0].equalsIgnoreCase("2")) {
					if (plugin.InGame2 == true) {
						plugin.Goals2gold = 0;
						plugin.Goals2purple = 0;
						BukkitTask GameStart2 = new GameEnd2(plugin)
								.runTaskTimer(plugin, 0, -1);
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You ended the game in Arena 2!");
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That game is not currently in progress!");
					}
				}
			}
		} else {
			player.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to do this!");
		}
		return false;
	}
}
