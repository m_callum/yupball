package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.kitteh.tag.TagAPI;

import com.myxcoding.yupie_123.YuPball;

public class Leave implements CommandExecutor {

	private YuPball plugin;

	public Leave(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (plugin.redTeam.contains(player)) {
				plugin.redTeam.remove(player);
				plugin.redTeamAmount--;
				player.sendMessage(plugin.message + ChatColor.GOLD
						+ "You have been removed from the "
						+ ChatColor.DARK_RED + "Red" + ChatColor.GOLD
						+ " team!");
				TagAPI.refreshPlayer(player);
				if (plugin.redTeamAmount == 0) {
					Bukkit.broadcastMessage(plugin.message + ChatColor.GREEN
							+ "Blue Team won on Arena 1");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forceend 1");
					for (Player p : plugin.redTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesLost");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesLost",
								won + 1);
					}
					for (Player p : plugin.blueTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesWon");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesWon",
								won + 1);
					}
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forcestart 1");
				}
			} else if (plugin.blueTeam.contains(player)) {
				plugin.blueTeam.remove(player);
				plugin.blueTeamAmount--;
				player.sendMessage(plugin.message + ChatColor.GOLD
						+ "You have been removed from the " + ChatColor.BLUE
						+ "Blue" + ChatColor.GOLD + " team!");
				if (plugin.blueTeamAmount == 0) {
					Bukkit.broadcastMessage(plugin.message + ChatColor.GREEN
							+ "Red Team won on Arena 1");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forceend 1");
					for (Player p : plugin.blueTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesLost");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesLost",
								won + 1);
					}
					for (Player p : plugin.redTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesWon");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesWon",
								won + 1);
					}
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forcestart 1");
				}
			} else if (plugin.purpleTeam.contains(player)) {
				plugin.purpleTeam.remove(player);
				plugin.purpleTeamAmount--;
				player.sendMessage(plugin.message + ChatColor.GOLD
						+ "You have been removed from the "
						+ ChatColor.LIGHT_PURPLE + "Purple" + ChatColor.GOLD
						+ " team!");
				if (plugin.purpleTeamAmount == 0) {
					Bukkit.broadcastMessage(plugin.message + ChatColor.GREEN
							+ "Red Team won on Arena 2");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forceend 1");
					for (Player p : plugin.purpleTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesLost");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesLost",
								won + 1);
					}
					for (Player p : plugin.goldTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesWon");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesWon",
								won + 1);
					}
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forcestart 2");
				}
			} else if (plugin.goldTeam.contains(player)) {
				plugin.goldTeam.remove(player);
				plugin.goldTeamAmount--;
				player.sendMessage(plugin.message + ChatColor.GOLD
						+ "You have been removed from the " + ChatColor.GOLD
						+ "Gold" + ChatColor.GOLD + " team!");
				if (plugin.goldTeamAmount == 0) {
					Bukkit.broadcastMessage(plugin.message + ChatColor.GREEN
							+ "Gold Team won on Arena 2");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forceend 1");
					for (Player p : plugin.goldTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesLost");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesLost",
								won + 1);
					}
					for (Player p : plugin.purpleTeam) {
						int won = plugin.getStats().getInt(
								"Data.Stats." + p.getName() + ".MatchesWon");
						plugin.getStats().set(
								"Data.Stats." + p.getName() + ".MatchesWon",
								won + 1);
					}
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
							"forcestart 2");
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.RED
						+ "You are not in a team!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.RED
					+ "You need to be ingame to perform this command!");
		}
		return false;
	}

}
