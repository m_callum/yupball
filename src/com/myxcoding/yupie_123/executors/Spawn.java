package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.myxcoding.yupie_123.YuPball;

public class Spawn implements CommandExecutor {

	private YuPball plugin;

	public Spawn(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
				if (plugin.getConfig().contains("Spawn")) {
					double X = plugin.getConfig().getDouble("Spawn.X");
					double Y = plugin.getConfig().getDouble("Spawn.Y");
					double Z = plugin.getConfig().getDouble("Spawn.Z");
					String world = plugin.getConfig().getString("Spawn.World");
					float Yaw = plugin.getConfig().getInt("Spawn.Yaw");
					float Pitch = plugin.getConfig().getInt("Spawn.Pitch");
					Location spawn = new Location(plugin.getServer().getWorld(
							world), X, Y, Z, Yaw, Pitch);
					player.getInventory().clear();
					player.getInventory().setItem(17,
							new ItemStack(Material.EMERALD, 1));
					player.teleport(spawn);
					Bukkit.dispatchCommand(player, "leave");
					if (plugin.Arena1.contains(player)) {
						Bukkit.dispatchCommand(player, "arena leave 1");
					} else if (plugin.Arena2.contains(player)) {
						Bukkit.dispatchCommand(player, "arena leave 2");
					}
					player.sendMessage(plugin.message + ChatColor.GOLD
							+ "You were teleported to the Server spawn!");
				} else {
					Location spawn = player.getWorld().getSpawnLocation();
					player.teleport(spawn);
					player.sendMessage(plugin.message + ChatColor.GOLD
							+ "You were teleported to the world's spawn!");
				}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}
}
