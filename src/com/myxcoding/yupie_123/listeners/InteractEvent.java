package com.myxcoding.yupie_123.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.myxcoding.yupie_123.YuPball;

public abstract class InteractEvent implements Listener, Plugin {

	private YuPball plugin;

	public InteractEvent(YuPball plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		int blockID = player.getItemInHand().getTypeId();
		if (event.getPlayer().getItemInHand().getType() == Material.DIAMOND_SWORD) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR
					|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				PotionEffect slowness6 = new PotionEffect(
						PotionEffectType.SLOW, 100000, 19);
				if (plugin.zoomedIn.get(player.getName()) == false) {
					player.addPotionEffect(slowness6);
					plugin.zoomedIn.put(player.getName(), true);
				} else if (plugin.zoomedIn.get(player.getName()) == true) {
					player.removePotionEffect(PotionEffectType.SLOW);
					plugin.zoomedIn.put(player.getName(), false);

				}
			} else if (event.getAction() == Action.LEFT_CLICK_AIR
					|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
				Block block = player.getTargetBlock(null, 50);
				final Material material = block.getType();
				final Location location = block.getLocation();
				final World world = block.getWorld();
				location.getBlock().setType(Material.DRAGON_EGG);
				Bukkit.getServer().getScheduler()
						.scheduleSyncDelayedTask(this, new Runnable() {
							@Override
							public void run() {
								world.createExplosion(location, 3, false);
								location.getBlock().setType(material);
							}

						}, 700L);
			}
		}
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			if (b.getType() == Material.SIGN
					|| b.getType() == Material.SIGN_POST
					|| b.getType() == Material.WALL_SIGN) {
				Sign s = (Sign) event.getClickedBlock().getState();
				String[] lines = s.getLines();
				if (lines[0].equalsIgnoreCase("")) {
					if (lines[1].equalsIgnoreCase(ChatColor.UNDERLINE + ""
							+ ChatColor.BOLD + "Back to")) {
						if (lines[2].equalsIgnoreCase(ChatColor.UNDERLINE + ""
								+ ChatColor.BOLD + "Spawn")) {
							double X = plugin.getConfig().getDouble("Spawn.X");
							double Y = plugin.getConfig().getDouble("Spawn.Y");
							double Z = plugin.getConfig().getDouble("Spawn.Z");
							String world = plugin.getConfig().getString(
									"Spawn.World");
							float Yaw = plugin.getConfig().getInt("Spawn.Yaw");
							float Pitch = plugin.getConfig().getInt(
									"Spawn.Pitch");
							Location spawn = new Location(plugin.getServer()
									.getWorld(world), X, Y, Z, Yaw, Pitch);
							player.teleport(spawn);
							player.sendMessage(plugin.message
									+ ChatColor.GOLD
									+ "You were teleported to the Server spawn!");
						}
					}
				} else if (lines[0].equalsIgnoreCase(ChatColor.DARK_GRAY + "["
						+ ChatColor.GOLD + "YuPball" + ChatColor.DARK_GRAY
						+ "]")) {
					if (lines[1].equalsIgnoreCase(ChatColor.BOLD + "Arena 1")) {
						if (lines[2]
								.equalsIgnoreCase(ChatColor.ITALIC + "Join")) {
							if (plugin.redTeam.contains(player)
									|| plugin.blueTeam.contains(player)
									|| plugin.purpleTeam.contains(player)
									|| plugin.goldTeam.contains(player)) {
								player.sendMessage(plugin.message
										+ ChatColor.DARK_RED
										+ "You cannot join a team again!");
							} else {
								if (plugin.InGame1 == true) {
									player.sendMessage(plugin.message
											+ ChatColor.DARK_RED
											+ "You cannot join a game in progress, Sorry!");
								} else {
									Bukkit.getServer().dispatchCommand(player,
											"join");
								}
							}
						}
					} else if (lines[1].equalsIgnoreCase(ChatColor.BOLD
							+ "Arena 2")) {
						if (lines[2]
								.equalsIgnoreCase(ChatColor.ITALIC + "Join")) {
							if (plugin.redTeam.contains(player)
									|| plugin.blueTeam.contains(player)
									|| plugin.purpleTeam.contains(player)
									|| plugin.goldTeam.contains(player)) {
								player.sendMessage(plugin.message
										+ ChatColor.DARK_RED
										+ "You cannot join a team again!");
							} else {
								if (plugin.InGame2 == true) {
									player.sendMessage(plugin.message
											+ ChatColor.DARK_RED
											+ "You cannot join a game in progress, Sorry!");
								} else {
									Bukkit.getServer().dispatchCommand(player,
											"join");
								}
							}
						}
					}
					if (lines[2].equalsIgnoreCase("Arena 1")) {
						if (plugin.getConfig().contains("Arena.Arena 1.Lobby")) {
							String world = plugin.getConfig().getString(
									"Arena.Arena 1.Lobby.World");
							Double X = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.X");
							Double Y = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Y");
							Double Z = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Z");
							float Yaw = plugin.getConfig().getInt(
									"Arena.Arena 1.Lobby.Yaw");
							float Pitch = plugin.getConfig().getInt(
									"Arena.Arena 1.Lobby.Pitch");
							Location lobby = new Location(plugin.getServer()
									.getWorld(world), X, Y, Z, Yaw, Pitch);
							Bukkit.getServer().createWorld(
									new WorldCreator(world));
							player.teleport(lobby);
							player.sendMessage(plugin.message + ChatColor.GOLD
									+ "You were teleported to Arena 1's Lobby");
							Bukkit.getServer().dispatchCommand(player,
									"arena join 1");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "That lobby has not been created yet!");
						}
					} else if (lines[2].equalsIgnoreCase("Arena 2")) {
						if (plugin.getConfig().contains("Arena.Arena 2.Lobby")) {
							String world = plugin.getConfig().getString(
									"Arena.Arena 2.Lobby.World");
							Double X = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.X");
							Double Y = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Y");
							Double Z = plugin.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Z");
							float Yaw = plugin.getConfig().getInt(
									"Arena.Arena 2.Lobby.Yaw");
							float Pitch = plugin.getConfig().getInt(
									"Arena.Arena 2.Lobby.Pitch");
							Location lobby = new Location(plugin.getServer()
									.getWorld(world), X, Y, Z, Yaw, Pitch);
							Bukkit.getServer().createWorld(
									new WorldCreator(world));
							player.teleport(lobby);
							player.sendMessage(plugin.message + ChatColor.GOLD
									+ "You were teleported to Arena 2's Lobby");
							Bukkit.getServer().dispatchCommand(player,
									"arena join 2");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "That lobby has not been created yet!");
						}
					}
				}
				if (blockID == 280) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK
							|| event.getAction() == Action.RIGHT_CLICK_AIR) {
						player.launchProjectile(Fireball.class);
						player.playSound(player.getLocation(),
								Sound.WITHER_SHOOT, 2F, 2F);
						player.getInventory().removeItem(
								new ItemStack(Material.STICK, 1));
					}
				} else if (blockID == 288) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK
							|| event.getAction() == Action.RIGHT_CLICK_AIR) {
						int duration_seconds = 120;
						PotionEffect Speed = new PotionEffect(
								PotionEffectType.SPEED,
								(duration_seconds * 20), 1);
						player.addPotionEffect(Speed);
						ItemStack Speed1 = new ItemStack(Material.FEATHER, 1);
						ItemMeta Speed1M = Speed1.getItemMeta();
						Speed1M.setDisplayName(ChatColor.AQUA + "Speed (2:00)");
						List<String> Speed1S = new ArrayList<String>();
						Speed1S.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Richt click to consume!");
						Speed1M.setLore(Speed1S);
						Speed1.setItemMeta(Speed1M);
						player.getInventory().removeItem(Speed1);
					}
				} else if (blockID == 287) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK
							|| event.getAction() == Action.RIGHT_CLICK_AIR) {
						int duration_seconds = 120;
						PotionEffect Jump = new PotionEffect(
								PotionEffectType.JUMP, (duration_seconds * 20),
								1);
						player.addPotionEffect(Jump);
						ItemStack Speed = new ItemStack(Material.STRING, 1);
						ItemMeta SpeedM = Speed.getItemMeta();
						SpeedM.setDisplayName(ChatColor.AQUA + "Jump (2:00)");
						List<String> SpeedS = new ArrayList<String>();
						SpeedS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
								+ "Right click to consume!");
						SpeedM.setLore(SpeedS);
						Speed.setItemMeta(SpeedM);
						player.getInventory().removeItem(Speed);
					}
				} else if (player.getItemInHand().getType() == Material.IRON_SWORD) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK
							|| event.getAction() == Action.RIGHT_CLICK_AIR) {
						player.launchProjectile(Fireball.class);
						player.playSound(player.getLocation(),
								Sound.WITHER_SHOOT, 2F, 2F);
					}
				} else if (player.getItemInHand().getType() == Material.BOOK) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK
							|| event.getAction() == Action.RIGHT_CLICK_AIR) {
						if (plugin.redTeam.contains(player)) {
							for (Player p : plugin.blueTeam) {
								p.hidePlayer(player);
							}
						} else if (plugin.blueTeam.contains(player)) {
							for (Player p : plugin.redTeam) {
								p.hidePlayer(player);
							}
						} else if (plugin.purpleTeam.contains(player)) {
							for (Player p : plugin.goldTeam) {
								p.hidePlayer(player);
							}
						} else if (plugin.goldTeam.contains(player)) {
							for (Player p : plugin.purpleTeam) {
								p.hidePlayer(player);
							}
						}

						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {

									@Override
									public void run() {
										for (Player p : Bukkit
												.getOnlinePlayers()) {
											p.showPlayer(player);
										}
									}

								}, 200L);
					}
				}
			}
		}
	}

}
