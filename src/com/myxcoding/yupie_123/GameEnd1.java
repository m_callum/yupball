package com.myxcoding.yupie_123;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameEnd1 extends BukkitRunnable {

	private YuPball plugin;

	public GameEnd1(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		for (final Player p : plugin.Arena1) {
			p.sendMessage(plugin.message + ChatColor.GREEN + "Game Ended!");
			p.getInventory().clear();
			plugin.InGame1 = false;
			plugin.saveConfig();
			plugin.getStats().set(
					"Data.Stats." + p.getName() + ".DeathsThisRound", 0);
			plugin.saveStats();
			Bukkit.getServer().getScheduler()
					.scheduleSyncDelayedTask(plugin, new Runnable() {
						@Override
						public void run() {
							double X = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.X");
							double Y = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Y");
							double Z = plugin.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Z");
							float Yaw = plugin.getConfig().getInt(
									"Arena.Arena 1.Lobby.Yaw");
							float Pitch = plugin.getConfig().getInt(
									"Arena.Arena 1.Lobby.Pitch");
							String world = plugin.getConfig().getString(
									"Arena.Arena 1.Lobby.World");
							Location location = new Location(plugin.getServer()
									.getWorld(world), X, Y, Z, Yaw, Pitch);
							p.teleport(location);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "Taking you back to the Lobby!");
						}

					}, (20 * 30));
		}
	}

}
