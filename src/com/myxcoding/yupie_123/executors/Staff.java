package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Staff implements CommandExecutor {

	private YuPball plugin;

	public Staff(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		Player player = (Player) sender;
			player.sendMessage(ChatColor.AQUA + "Staff Online:");
			player.sendMessage(ChatColor.YELLOW + "Mods: " + ChatColor.GRAY
					+ plugin.mods);
			player.sendMessage(ChatColor.GREEN + "Admins: " + ChatColor.GRAY
					+ plugin.admins);
		return false;
	}

}
