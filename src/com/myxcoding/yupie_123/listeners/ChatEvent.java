package com.myxcoding.yupie_123.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.myxcoding.yupie_123.YuPball;

public class ChatEvent implements Listener {

	private YuPball plugin;

	public ChatEvent(YuPball plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onChatEvent(AsyncPlayerChatEvent event) {
		if (!event.getPlayer().hasPermission("yupball.links")) {
			if (event.getMessage().contains("http://")) {
				event.setCancelled(true);
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"warn " + event.getPlayer().getName());
			} else if (event.getMessage().contains("www.")) {
				event.setCancelled(true);
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"warn " + event.getPlayer().getName());
			} else if (event.getMessage().contains(".com")) {
				event.setCancelled(true);
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"warn " + event.getPlayer().getName());
			}
			for (String word : plugin.getConfig().getStringList("Banned Words")) {
				if (event.getMessage().contains(word)) {
					event.setCancelled(true);
					Bukkit.getServer().dispatchCommand(
							Bukkit.getConsoleSender(),
							"warn " + event.getPlayer().getName());
				}
			}
		}
		if (plugin.StaffChat.contains(event.getPlayer())) {
			Player player = event.getPlayer();
			event.setCancelled(true);
			for (Player p : plugin.StaffChat) {
				if (player.hasPermission("yupball.staff.mod")
						|| player.hasPermission("yupball.staff.admin")
						|| player.hasPermission("yupball.staff.coowner")
						|| player.hasPermission("yupball.staff.owner")) {
					p.sendMessage(player.getDisplayName() + ": "
							+ ChatColor.GREEN + event.getMessage());
				} else {
					p.sendMessage(player.getDisplayName() + ": "
							+ ChatColor.GRAY + event.getMessage());
				}
			}
		} else if (plugin.DonatorChat.contains(event.getPlayer())) {
			Player player = event.getPlayer();
			event.setCancelled(true);
			for (Player p : plugin.DonatorChat) {
				if (player.hasPermission("yupball.staff.mod")
						|| player.hasPermission("yupball.staff.admin")
						|| player.hasPermission("yupball.staff.coowner")
						|| player.hasPermission("yupball.staff.owner")) {
					p.sendMessage(player.getDisplayName() + ": "
							+ ChatColor.GREEN + event.getMessage());
				} else {
					p.sendMessage(player.getDisplayName() + ": "
							+ ChatColor.GRAY + event.getMessage());
				}
			}
		}
		if (plugin.getStats().getBoolean(
				"Data.Stats." + event.getPlayer().getName() + ".Muted") == true) {
			event.setCancelled(true);
		}
	}

}
