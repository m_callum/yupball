package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Spawnmob implements CommandExecutor {

	private YuPball plugin;

	public Spawnmob(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length < 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /spawnmob <Mob Type>");
			} else if (args.length > 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /spawnmob <Mob Type>");
			} else {
				EntityType type = null;

				try {
					type = EntityType.valueOf(args[0]);
					Location l = player.getEyeLocation();
					l.getWorld().spawnEntity(l, type);
				} catch (Exception e) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Please use a correct mob type!");
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
