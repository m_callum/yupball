package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Maps implements CommandExecutor {

	private YuPball plugin;

	public Maps(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.map.set")) {
				if (args.length < 2) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /map <Map no.> <Team1/Team2>");
					if (args.length == 2) {
						if (plugin.getMaps().contains("Map" + args[0])) {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "That ID already exists!");
						} else {
							int number = Integer.parseInt(args[0]);
							if (args[1].equalsIgnoreCase("team1")) {
								plugin.getMaps().set(
										"Map" + number + ".Team1.World",
										player.getWorld().getName());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team1.X",
										player.getLocation().getX());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team1.Y",
										player.getLocation().getY());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team1.Z",
										player.getLocation().getZ());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team1.Yaw",
										player.getLocation().getYaw());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team1.Pitch",
										player.getLocation().getPitch());
								plugin.saveMaps();
								player.sendMessage(plugin.message
										+ ChatColor.GOLD
										+ "You imported the map "
										+ ChatColor.GREEN
										+ player.getWorld().getName());
							} else if (args[1].equalsIgnoreCase("Team2")) {
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.World",
										player.getWorld().getName());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.X",
										player.getLocation().getX());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.Y",
										player.getLocation().getY());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.Z",
										player.getLocation().getZ());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.Yaw",
										player.getLocation().getYaw());
								plugin.getMaps().set(
										"Map" + args[0] + ".Team2.Pitch",
										player.getLocation().getPitch());
								plugin.saveMaps();
								player.sendMessage(plugin.message
										+ ChatColor.GOLD
										+ "You imported the map "
										+ ChatColor.GREEN
										+ player.getWorld().getName());
							}
						}
					}
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to do this!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be ingame to perform this command!");
		}
		return false;
	}
}
