package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Msg implements CommandExecutor {

	private YuPball plugin;

	public Msg(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
				if (args.length < 2) {
					p.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /msg <Player> <Message>");
				} else {
					Player targetPlayer = plugin.getServer().getPlayer(args[0]);
					StringBuilder sb = new StringBuilder();
					for (String s : args) {
						if (!s.equalsIgnoreCase(args[0])) {
							sb.append(" " + s);
						}
					}
					if (targetPlayer == null) {
						p.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That player is not on the server!");
					} else {
						targetPlayer.sendMessage(ChatColor.GOLD + "["
								+ ChatColor.GRAY + p.getName()
								+ ChatColor.GREEN + " -> " + ChatColor.GRAY
								+ "Me" + ChatColor.GOLD + "]:" + ChatColor.BLUE
								+ sb);
						p.sendMessage(ChatColor.GOLD + "["
								+ ChatColor.GRAY + "Me"
								+ ChatColor.GREEN + " -> " + ChatColor.GRAY
								+ targetPlayer.getName() + ChatColor.GOLD + "]:" + ChatColor.BLUE
								+ sb);
					}
				}

		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
