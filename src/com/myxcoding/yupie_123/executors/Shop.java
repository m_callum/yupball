package com.myxcoding.yupie_123.executors;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.myxcoding.yupie_123.YuPball;

public class Shop implements CommandExecutor {

	private YuPball plugin;

	public Shop(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

				if (player.getWorld().getName().equalsIgnoreCase("Spawn")
						|| player.getWorld().getName()
								.equalsIgnoreCase("Lobby")
						|| player.getWorld().getName()
								.equalsIgnoreCase("Lobby1")) {
					player.sendMessage(plugin.message + ChatColor.YELLOW
							+ "You can only open the shop ingame!");
					return true;
				} else {
					// Iron
					ItemStack Iron = new ItemStack(Material.IRON_INGOT, 1);
					ItemMeta IronM = Iron.getItemMeta();
					IronM.setDisplayName(ChatColor.AQUA + "10 Snowballs");
					List<String> IronS = new ArrayList<String>();
					IronS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "50 Points");
					IronM.setLore(IronS);
					Iron.setItemMeta(IronM);
					// Gold
					ItemStack Gold = new ItemStack(Material.GOLD_INGOT, 1);
					ItemMeta GoldM = Gold.getItemMeta();
					GoldM.setDisplayName(ChatColor.AQUA + "50 Snowballs");
					List<String> GoldS = new ArrayList<String>();
					GoldS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "200 Points");
					GoldM.setLore(GoldS);
					Gold.setItemMeta(GoldM);
					// Diamond
					ItemStack Diamond = new ItemStack(Material.DIAMOND, 1);
					ItemMeta DiamondM = Diamond.getItemMeta();
					DiamondM.setDisplayName(ChatColor.AQUA + "100 Snowballs");
					List<String> DiamondS = new ArrayList<String>();
					DiamondS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "300 Points");
					DiamondM.setLore(DiamondS);
					Diamond.setItemMeta(DiamondM);
					// Iron_Plate
					ItemStack Iron_Plate = new ItemStack(Material.STONE_PLATE,
							1);
					ItemMeta Iron_PM = Iron_Plate.getItemMeta();
					Iron_PM.setDisplayName(ChatColor.AQUA + "Mine");
					List<String> Iron_PS = new ArrayList<String>();
					Iron_PS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "400 Points");
					Iron_PM.setLore(Iron_PS);
					Iron_Plate.setItemMeta(Iron_PM);
					// Bomb(Egg)
					ItemStack Egg = new ItemStack(Material.EGG, 1);
					ItemMeta EggM = Egg.getItemMeta();
					EggM.setDisplayName(ChatColor.AQUA + "Bomb");
					List<String> EggS = new ArrayList<String>();
					EggS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "200 Points");
					EggM.setLore(EggS);
					Egg.setItemMeta(EggM);
					// Speed
					ItemStack Speed = new ItemStack(Material.FEATHER, 1);
					ItemMeta SpeedM = Speed.getItemMeta();
					SpeedM.setDisplayName(ChatColor.AQUA + "Speed (2:00)");
					List<String> SpeedS = new ArrayList<String>();
					SpeedS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "200 Points");
					SpeedM.setLore(SpeedS);
					Speed.setItemMeta(SpeedM);
					// Jump
					ItemStack Jump = new ItemStack(Material.STRING, 1);
					ItemMeta JumpM = Jump.getItemMeta();
					JumpM.setDisplayName(ChatColor.AQUA + "Jump (2:00)");
					List<String> JumpS = new ArrayList<String>();
					JumpS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "300 Points");
					JumpM.setLore(JumpS);
					Jump.setItemMeta(JumpM);
					// Rocket Launcher(Stick)
					ItemStack Stick = new ItemStack(Material.STICK, 1);
					ItemMeta StickM = Stick.getItemMeta();
					StickM.setDisplayName(ChatColor.AQUA + "Rocket Launcher");
					List<String> StickS = new ArrayList<String>();
					StickS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
							+ "500 Points");
					StickM.setLore(StickS);
					Stick.setItemMeta(StickM);
					player.openInventory(YuPball.Shop);
					YuPball.Shop.clear();
					YuPball.Shop.setItem(0, Iron);
					YuPball.Shop.setItem(1, Gold);
					YuPball.Shop.setItem(2, Diamond);
					YuPball.Shop.setItem(3, Egg);
					YuPball.Shop.setItem(4, Speed);
					YuPball.Shop.setItem(5, Jump);
					YuPball.Shop.setItem(6, Iron_Plate);
					YuPball.Shop.setItem(7, Stick);
					player.setNoDamageTicks(-1);
				}

		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame to player!");
		}
		return false;
	}

}
