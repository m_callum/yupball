package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Invsee implements CommandExecutor {

	private YuPball plugin;

	public Invsee(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.invsee")) {
				if (args.length < 1) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /invsee <Player>");
				} else if (args.length > 1) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /invsee <Player>");
				} else {
					Player targetPlayer = plugin.getServer().getPlayer(args[0]);
					player.openInventory(targetPlayer.getInventory());
					player.sendMessage(plugin.message + ChatColor.GOLD
							+ "You have opened " + targetPlayer.getName()
							+ "'s Inventory!");
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to perform this command!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
