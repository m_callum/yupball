package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Mute implements CommandExecutor {

	private YuPball plugin;

	public Mute(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender.hasPermission("yupball.mute")) {
			if (args.length == 1) {
				Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
				if (targetPlayer != null) {
					plugin.getStats().set(
							"Data.Stats." + targetPlayer.getName() + ".Muted",
							true);
					sender.sendMessage(plugin.message + ChatColor.GREEN
							+ "You muted " + args[0]);
					targetPlayer
							.sendMessage(plugin.message
									+ ChatColor.YELLOW
									+ "You have been muted by a member of staff, to be unmuted you need to go to the website and purchase unmuting in the unlocking tab!");
				} else {
					sender.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "That player is not online!");
				}
			} else {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /mute <Player>");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to do this!");
		}
		return false;
	}

}
