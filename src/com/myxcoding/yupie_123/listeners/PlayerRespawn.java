package com.myxcoding.yupie_123.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.myxcoding.yupie_123.YuPball;

public class PlayerRespawn implements Listener {

	private YuPball plugin;

	public PlayerRespawn(YuPball plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void playerRespawn(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(plugin, new Runnable() {
					@Override
					public void run() {
						if (plugin.Arena1.contains(player)
								|| plugin.Arena2.contains(player)) {
							if (plugin.getStats().getInt(
									"Data.Stats." + player.getName()
											+ ".DeathsThisRound") == 1
									|| plugin.getStats().getInt(
											"Data.Stats." + player.getName()
													+ ".DeathsThisRound") == 2) {
								if (plugin.Arena1.contains(player)) {
									if (plugin.redTeam.contains(player)) {
										int ArenaTeam1 = plugin.getConfig()
												.getInt("Arena1MapID");
										double SpecX = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.X");
										double SpecY = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.Y");
										double SpecZ = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.Z");
										String World = plugin
												.getMaps()
												.getString(
														"Map"
																+ ArenaTeam1
																+ ".Team1.World");
										float Yaw = plugin.getMaps().getInt(
												"Map" + ArenaTeam1
														+ ".Team1.Yaw");
										float Pitch = plugin.getMaps().getInt(
												"Map" + ArenaTeam1
														+ ".Team1.Pitch");
										Location Spectate = new Location(Bukkit
												.getWorld(World), SpecX, SpecY,
												SpecZ, Yaw, Pitch);
										Bukkit.getServer().createWorld(
												new WorldCreator(World));
										player.teleport(Spectate);
										PotionEffect invulnerable = new PotionEffect(
												PotionEffectType.REGENERATION,
												60, 10);
										player.addPotionEffect(invulnerable);
									} else if (plugin.blueTeam.contains(player)) {
										int ArenaTeam2 = plugin.getConfig()
												.getInt("Arena1MapID");
										double SpecX = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.X");
										double SpecY = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.Y");
										double SpecZ = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.Z");
										String World = plugin
												.getMaps()
												.getString(
														"Map"
																+ ArenaTeam2
																+ ".Team2.World");
										float Yaw = plugin.getMaps().getInt(
												"Map" + ArenaTeam2
														+ ".Team2.Yaw");
										float Pitch = plugin.getMaps().getInt(
												"Map" + ArenaTeam2
														+ ".Team2.Pitch");
										Location Spectate = new Location(Bukkit
												.getWorld(World), SpecX, SpecY,
												SpecZ, Yaw, Pitch);
										Bukkit.getServer().createWorld(
												new WorldCreator(World));
										player.teleport(Spectate);
										PotionEffect invulnerable = new PotionEffect(
												PotionEffectType.REGENERATION,
												60, 10);
										player.addPotionEffect(invulnerable);
									}
								} else if (plugin.Arena2.contains(player)) {
									if (plugin.goldTeam.contains(player)) {
										int ArenaTeam1 = plugin.getConfig()
												.getInt("Arena2MapID");
										double SpecX = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.X");
										double SpecY = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.Y");
										double SpecZ = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam1
																+ ".Team1.Z");
										String World = plugin
												.getMaps()
												.getString(
														"Map"
																+ ArenaTeam1
																+ ".Team1.World");
										float Yaw = plugin.getMaps().getInt(
												"Map" + ArenaTeam1
														+ ".Team1.Yaw");
										float Pitch = plugin.getMaps().getInt(
												"Map" + ArenaTeam1
														+ ".Team1.Pitch");
										Location Spectate = new Location(Bukkit
												.getWorld(World), SpecX, SpecY,
												SpecZ, Yaw, Pitch);
										Bukkit.getServer().createWorld(
												new WorldCreator(World));
										player.teleport(Spectate);
										PotionEffect invulnerable = new PotionEffect(
												PotionEffectType.REGENERATION,
												60, 10);
										player.addPotionEffect(invulnerable);
									} else if (plugin.purpleTeam
											.contains(player)) {
										int ArenaTeam2 = plugin.getConfig()
												.getInt("Arena2MapID");
										double SpecX = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.X");
										double SpecY = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.Y");
										double SpecZ = plugin.getMaps()
												.getDouble(
														"Map" + ArenaTeam2
																+ ".Team2.Z");
										String World = plugin
												.getMaps()
												.getString(
														"Map"
																+ ArenaTeam2
																+ ".Team2.World");
										float Yaw = plugin.getMaps().getInt(
												"Map" + ArenaTeam2
														+ ".Team2.Yaw");
										float Pitch = plugin.getMaps().getInt(
												"Map" + ArenaTeam2
														+ ".Team2.Pitch");
										Location Spectate = new Location(Bukkit
												.getWorld(World), SpecX, SpecY,
												SpecZ, Yaw, Pitch);
										Bukkit.getServer().createWorld(
												new WorldCreator(World));
										player.teleport(Spectate);
										PotionEffect invulnerable = new PotionEffect(
												PotionEffectType.REGENERATION,
												60, 10);
										player.addPotionEffect(invulnerable);
									}
								}
							} else if (plugin.getStats().getInt(
									"Data.Stats." + player.getName()
											+ ".DeathsThisRound") == 3) {
								if (plugin.Arena1.contains(player)) {
									int mapID = plugin.getConfig().getInt(
											"Arena1MapID");
									double SpecX = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.X");
									double SpecY = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.Y");
									double SpecZ = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.Z");
									String World = plugin.getMaps().getString(
											"Map" + mapID + ".Spectate.World");
									float Yaw = plugin.getMaps().getInt(
											"Map" + mapID + ".Spectate.Yaw");
									float Pitch = plugin.getMaps().getInt(
											"Map" + mapID + ".Spectate.Pitch");
									Location Spectate = new Location(Bukkit
											.getWorld(World), SpecX, SpecY,
											SpecZ, Yaw, Pitch);
									Bukkit.getServer().createWorld(
											new WorldCreator(World));
									player.teleport(Spectate);
									player.sendMessage(plugin.message
											+ ChatColor.GOLD
											+ "You are now out of the game!");
									plugin.getMaps().set(
											"Data.Stats." + player.getName()
													+ ".DeathsThisRound", 0);
								} else if (plugin.Arena2.contains(player)) {
									int mapID = plugin.getMaps().getInt(
											"Arena2MapID");
									double SpecX = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.X");
									double SpecY = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.Y");
									double SpecZ = plugin.getMaps().getDouble(
											"Map" + mapID + ".Spectate.Z");
									String World = plugin.getMaps().getString(
											"Map" + mapID + ".Spectate.World");
									float Yaw = plugin.getMaps().getInt(
											"Map" + mapID + ".Spectate.Yaw");
									float Pitch = plugin.getMaps().getInt(
											"Map" + mapID + ".Spectate.Pitch");
									Location Spectate = new Location(Bukkit
											.getWorld(World), SpecX, SpecY,
											SpecZ, Yaw, Pitch);
									Bukkit.getServer().createWorld(
											new WorldCreator(World));
									player.teleport(Spectate);
									player.sendMessage(plugin.message
											+ ChatColor.GOLD
											+ "You are now out of the game!");
									plugin.getMaps().set(
											"Data.Stats." + player.getName()
													+ ".DeathsThisRound", 0);
									Bukkit.dispatchCommand(player, "spawn");
								}
							}
						} else {
							Bukkit.dispatchCommand(player, "spawn");
						}

					}

				}, 20L);

	}

}
