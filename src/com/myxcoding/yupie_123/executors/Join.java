package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.kitteh.tag.TagAPI;

import com.myxcoding.yupie_123.YuPball;

public class Join implements CommandExecutor {

	private YuPball plugin;

	public Join(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (plugin.redTeam.contains(player.getName())) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You are already in a team!");
			} else if (plugin.blueTeam.contains(player.getName())) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You are already in a team!");
			} else if (plugin.purpleTeam.contains(player.getName())) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You are already in a team!");
			} else if (plugin.goldTeam.contains(player.getName())) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You are already in a team!");
			} else {
				if (plugin.Arena1.contains(player)) {
					if (plugin.redTeamAmount < plugin.blueTeamAmount) {
						plugin.redTeamAmount++;
						plugin.players1++;
						plugin.redTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.DARK_RED + "Red" + ChatColor.GRAY
								+ "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.DARK_RED + "Red " + ChatColor.GOLD
								+ "Team");
						TagAPI.refreshPlayer(player);
						if (plugin.getConfig().contains("Arena.1.RedSpawn.X")) {

						}
					} else if (plugin.blueTeamAmount < plugin.redTeamAmount) {
						plugin.blueTeamAmount++;
						plugin.players1++;
						plugin.blueTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.BLUE + "Blue" + ChatColor.GRAY
								+ "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.BLUE + "Blue " + ChatColor.GOLD
								+ "Team");
						TagAPI.refreshPlayer(player);
					} else if (plugin.redTeamAmount == plugin.blueTeamAmount) {
						plugin.redTeamAmount++;
						plugin.players1++;
						plugin.redTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.DARK_RED + "Red" + ChatColor.GRAY
								+ "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.DARK_RED + "Red " + ChatColor.GOLD
								+ "Team");
						TagAPI.refreshPlayer(player);
					}
				} else if (plugin.Arena2.contains(player)) {
					if (plugin.purpleTeamAmount < plugin.goldTeamAmount) {
						plugin.purpleTeamAmount++;
						plugin.players2++;
						plugin.purpleTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.LIGHT_PURPLE + "Purple"
								+ ChatColor.GRAY + "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.LIGHT_PURPLE + "Purple "
								+ ChatColor.GOLD + "Team");
						TagAPI.refreshPlayer(player);
					} else if (plugin.goldTeamAmount < plugin.purpleTeamAmount) {
						plugin.goldTeamAmount++;
						plugin.players2++;
						plugin.goldTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.GOLD + "Gold" + ChatColor.GRAY
								+ "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.GOLD + "Gold " + ChatColor.GOLD
								+ "Team");
						TagAPI.refreshPlayer(player);
					} else if (plugin.purpleTeamAmount == plugin.goldTeamAmount) {
						plugin.purpleTeamAmount++;
						plugin.players2++;
						plugin.purpleTeam.add(player);
						String name = player.getDisplayName();
						player.setDisplayName(ChatColor.GRAY + "["
								+ ChatColor.LIGHT_PURPLE + "Purple"
								+ ChatColor.GRAY + "]" + name + ChatColor.RESET);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been Placed in the "
								+ ChatColor.LIGHT_PURPLE + "Purple "
								+ ChatColor.GOLD + "Team");
						TagAPI.refreshPlayer(player);
					}
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player!");
		}
		return false;
	}
}
