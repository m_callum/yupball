package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class WorldCmd implements CommandExecutor {

	private YuPball plugin;

	public WorldCmd(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.world")) {
				if (args.length < 1) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage:/world <World name>");
				} else if (args.length == 1) {
					Bukkit.getServer().createWorld(new WorldCreator(args[0]));
					if (Bukkit.getWorld(args[0]) != null) {
						Location spawn = Bukkit.getWorld(args[0])
								.getSpawnLocation();
						player.teleport(spawn);
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That world does not exist!");
					}
				} else {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage:/world <World name>");
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to perform this command!");
			}
		}
		return false;
	}
}
