package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import com.myxcoding.yupie_123.YuPball;

public class YB implements CommandExecutor {

	private YuPball plugin;

	public YB(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
				PluginDescriptionFile pdfFile = plugin.getDescription();
				player.sendMessage(ChatColor.AQUA + "YuPball was created by: "
						+ ChatColor.GOLD + pdfFile.getAuthors());
				player.sendMessage(ChatColor.AQUA + "YuPball was designed by: "
						+ ChatColor.GOLD + "Yupie_123 (Owner of YuPcraft)");
		}
		return false;
	}

}
