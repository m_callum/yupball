package com.myxcoding.yupie_123;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.minecraft.server.v1_5_R3.Packet205ClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.kitteh.tag.PlayerReceiveNameTagEvent;
import org.mcsg.double0negative.tabapi.TabAPI;

import com.myxcoding.yupie_123.executors.Arena;
import com.myxcoding.yupie_123.executors.Away;
import com.myxcoding.yupie_123.executors.ChatChannel;
import com.myxcoding.yupie_123.executors.CommandCmd;
import com.myxcoding.yupie_123.executors.Donate;
import com.myxcoding.yupie_123.executors.DonatorAdd;
import com.myxcoding.yupie_123.executors.EditStats;
import com.myxcoding.yupie_123.executors.FireballCmd;
import com.myxcoding.yupie_123.executors.ForceEnd;
import com.myxcoding.yupie_123.executors.ForceStart;
import com.myxcoding.yupie_123.executors.Invsee;
import com.myxcoding.yupie_123.executors.Join;
import com.myxcoding.yupie_123.executors.Kit;
import com.myxcoding.yupie_123.executors.Leave;
import com.myxcoding.yupie_123.executors.Map;
import com.myxcoding.yupie_123.executors.Msg;
import com.myxcoding.yupie_123.executors.Mute;
import com.myxcoding.yupie_123.executors.Re;
import com.myxcoding.yupie_123.executors.Score;
import com.myxcoding.yupie_123.executors.SetSpawn;
import com.myxcoding.yupie_123.executors.Shop;
import com.myxcoding.yupie_123.executors.Site;
import com.myxcoding.yupie_123.executors.Spawn;
import com.myxcoding.yupie_123.executors.Staff;
import com.myxcoding.yupie_123.executors.Stats;
import com.myxcoding.yupie_123.executors.UnMute;
import com.myxcoding.yupie_123.executors.WorldCmd;
import com.myxcoding.yupie_123.executors.YB;
import com.myxcoding.yupie_123.executors.warnExecutor;
import com.myxcoding.yupie_123.listeners.BlockBreak;
import com.myxcoding.yupie_123.listeners.BlockPlace;
import com.myxcoding.yupie_123.listeners.ChatEvent;
import com.myxcoding.yupie_123.listeners.FoodChange;
import com.myxcoding.yupie_123.listeners.InventoryClick;
import com.myxcoding.yupie_123.listeners.InventoryClose;
import com.myxcoding.yupie_123.listeners.OpenInventory;
import com.myxcoding.yupie_123.listeners.PickupItem;
import com.myxcoding.yupie_123.listeners.PlayerJoin;
import com.myxcoding.yupie_123.listeners.PlayerRespawn;

public class YuPball extends JavaPlugin implements Listener {
	public static final Logger log = Logger.getLogger("Minecraft");
	public final BlockBreak BlockBreak = new BlockBreak();
	public final PlayerJoin PlayerJoin = new PlayerJoin(this);
	public final InventoryClick InventoryClick = new InventoryClick(this);
	public final BlockPlace BlockPlace = new BlockPlace();
	public final FoodChange FoodChange = new FoodChange();
	public final OpenInventory OpenInventory = new OpenInventory(this);
	public final ChatEvent ChatEvent = new ChatEvent(this);
	public final InventoryClose InventoryClose = new InventoryClose();
	public final PickupItem PickupItem = new PickupItem();
	public final PlayerRespawn PlayerRespawn = new PlayerRespawn(this);
	public boolean InGame1;
	public boolean InGame2;
	public int redTeamAmount;
	public int blueTeamAmount;
	public int purpleTeamAmount;
	public int goldTeamAmount;
	public int players1;
	public int players2;
	public int mapamount;
	public int Goals1red;
	public int Goals1blue;
	public int Goals2gold;
	public int Goals2purple;
	public HashMap<String, Location> loc1 = new HashMap<String, Location>();
	public HashMap<String, Location> loc2 = new HashMap<String, Location>();
	public HashMap<String, Integer> killstreak = new HashMap<String, Integer>();
	public HashMap<String, Boolean> zoomedIn = new HashMap<String, Boolean>();
	public HashMap<String, Integer> heavyDonatorTime = new HashMap<String, Integer>();
	public HashMap<String, Integer> doctorDonatorTime = new HashMap<String, Integer>();
	public ArrayList<String> staff = new ArrayList<String>();
	public ArrayList<String> admins = new ArrayList<String>();
	public ArrayList<String> mods = new ArrayList<String>();
	public ArrayList<Player> redTeam = new ArrayList<Player>();
	public ArrayList<Player> blueTeam = new ArrayList<Player>();
	public ArrayList<Player> purpleTeam = new ArrayList<Player>();
	public ArrayList<Player> goldTeam = new ArrayList<Player>();
	public ArrayList<Player> Arena1 = new ArrayList<Player>();
	public ArrayList<Player> Arena2 = new ArrayList<Player>();
	public ArrayList<Player> DonatorChat = new ArrayList<Player>();
	public ArrayList<Player> StaffChat = new ArrayList<Player>();
	public ArrayList<Player> NormalChat = new ArrayList<Player>();
	public ArrayList<Player> DoctorDonatorCD = new ArrayList<Player>();
	public ArrayList<Player> DoctorDonatorDone = new ArrayList<Player>();
	public HashMap<String, Integer> DoctorDonatorTimes = new HashMap<String, Integer>();
	public ArrayList<Player> HeavyDonatorCD = new ArrayList<Player>();
	public ArrayList<Player> HeavyDonatorDone = new ArrayList<Player>();
	public ArrayList<Player> SpyDisguise = new ArrayList<Player>();
	public ArrayList<Player> SniperCD = new ArrayList<Player>();
	public boolean inGame = false;
	public boolean inLobby = false;
	public static Inventory Shop;
	public Inventory test;
	public static ItemStack Slimeball = new ItemStack(Material.SLIME_BALL);
	public String ServerOffReason;
	public YamlConfiguration stats;
	public YamlConfiguration banned;
	public YamlConfiguration maps;
	File statsFile;
	File bannedFile;
	File mapsFile;
	public String message = ChatColor.RESET + "";
	public int map1;
	public int map2;
	private final static Random rand = new Random();

	// private GameStart1 gameStart;

	// public YuPball(GameStart1 gameStart) {
	// this.gameStart = gameStart;
	// }

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"gamerule keepInventory true");
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"gamerule doMobSpawning false");
		map2 = 1;
		map1 = 1;
		InGame1 = false;
		InGame2 = false;
		redTeamAmount = 0;
		blueTeamAmount = 0;
		saveConfig();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard teams = manager.getNewScoreboard();
		Team blue = teams.registerNewTeam("Blue");
		Team red = teams.registerNewTeam("Red");
		Team purple = teams.registerNewTeam("Purple");
		Team gold = teams.registerNewTeam("Gold");
		red.setAllowFriendlyFire(false);
		blue.setAllowFriendlyFire(false);
		purple.setAllowFriendlyFire(false);
		gold.setAllowFriendlyFire(false);
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(this, this);
		pm.registerEvents(this.BlockBreak, this);
		pm.registerEvents(this.PlayerJoin, this);
		pm.registerEvents(this.BlockPlace, this);
		pm.registerEvents(this.InventoryClick, this);
		pm.registerEvents(this.FoodChange, this);
		pm.registerEvents(this.OpenInventory, this);
		pm.registerEvents(this.ChatEvent, this);
		pm.registerEvents(this.InventoryClose, this);
		pm.registerEvents(this.PickupItem, this);
		pm.registerEvents(this.PlayerRespawn, this);
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info("[" + pdfFile.getName() + "] V" + pdfFile.getVersion()
				+ " Has Been Enabled!");
		try {
			saveConfig();
			setupConfig(getConfig());
			getConfig().options().copyDefaults(true);
			saveConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}

		statsFile = new File(getDataFolder(), "stats.yml");
		if (!statsFile.exists()) {
			try {
				statsFile.createNewFile();
			} catch (IOException e) {
				log.severe("[" + pdfFile.getName() + "]" + ChatColor.DARK_RED
						+ "Could not create stats file");
			}
		}

		bannedFile = new File(getDataFolder(), "bans.yml");
		if (!bannedFile.exists()) {
			try {
				bannedFile.createNewFile();
			} catch (IOException e) {
				log.severe("[" + pdfFile.getName() + "]" + ChatColor.DARK_RED
						+ "Could not create bans file");
			}
		}

		mapsFile = new File(getDataFolder(), "maps.yml");
		if (!mapsFile.exists()) {
			try {
				mapsFile.createNewFile();
			} catch (IOException e) {
				log.severe("[" + pdfFile.getName() + "]" + ChatColor.DARK_RED
						+ "Could not create maps file");
			}
		}

		stats = YamlConfiguration.loadConfiguration(statsFile);
		banned = YamlConfiguration.loadConfiguration(bannedFile);
		maps = YamlConfiguration.loadConfiguration(mapsFile);

		Bukkit.getServer().getScheduler()
				.scheduleAsyncRepeatingTask(this, new Runnable() {

					@Override
					public void run() {
						for (Player p : Bukkit.getOnlinePlayers()) {
							updateTabList(p);
						}
					}

				}, 0L, 100L);
		Shop = Bukkit.createInventory(null, 9, "Shop");
		test = Bukkit.createInventory(null, InventoryType.DROPPER);
		this.getCommand("warn").setExecutor(new warnExecutor(this));
		this.getCommand("join").setExecutor(new Join(this));
		this.getCommand("arena").setExecutor(new Arena(this));
		this.getCommand("donate").setExecutor(new Donate(this));
		this.getCommand("score").setExecutor(new Score(this));
		this.getCommand("world").setExecutor(new WorldCmd(this));
		this.getCommand("staff").setExecutor(new Staff(this));
		this.getCommand("editstats").setExecutor(new EditStats(this));
		this.getCommand("pl").setExecutor(new YB(this));
		this.getCommand("shop").setExecutor(new Shop(this));
		this.getCommand("leave").setExecutor(new Leave(this));
		this.getCommand("stats").setExecutor(new Stats(this));
		this.getCommand("spawn").setExecutor(new Spawn(this));
		this.getCommand("setspawn").setExecutor(new SetSpawn(this));
		this.getCommand("site").setExecutor(new Site(this));
		this.getCommand("away").setExecutor(new Away(this));
		this.getCommand("re").setExecutor(new Re(this));
		this.getCommand("fireball").setExecutor(new FireballCmd(this));
		this.getCommand("invsee").setExecutor(new Invsee(this));
		this.getCommand("forcestart").setExecutor(new ForceStart(this));
		this.getCommand("forceend").setExecutor(new ForceEnd(this));
		this.getCommand("map").setExecutor(new Map(this));
		this.getCommand("msg").setExecutor(new Msg(this));
		// this.getCommand("friends").setExecutor(new Friends(this));
		this.getCommand("ch").setExecutor(new ChatChannel(this));
		this.getCommand("command").setExecutor(new CommandCmd(this));
		this.getCommand("kit").setExecutor(new Kit(this));
		this.getCommand("mute").setExecutor(new Mute(this));
		this.getCommand("unmute").setExecutor(new UnMute(this));
		this.getCommand("donator").setExecutor(new DonatorAdd(this));

		if (getConfig().contains("ServerOffReason")) {
			ServerOffReason = getConfig().getString("ServerOffReason");
		} else {
			ServerOffReason = "Server Closed";
		}

		@SuppressWarnings("unused")
		BukkitTask TimeSet = new TimeSetRunnable().runTaskTimer(this, 20, 20);
	}

	// Stats
	public void saveStats() {
		if (stats == null || statsFile == null) {
			return;
		}
		try {
			getStats().save(statsFile);
		} catch (IOException ex) {
			this.getLogger().log(Level.SEVERE,
					"Could not save config to " + statsFile, ex);
		}
	}

	public FileConfiguration getStats() {
		if (stats == null) {
			this.reloadStats();
		}
		return stats;
	}

	public void reloadStats() {
		if (statsFile == null) {
			statsFile = new File(getDataFolder(), "stats.yml");
		}
		stats = YamlConfiguration.loadConfiguration(statsFile);
		InputStream defConfigStream = this.getResource("stats.yml");
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);
			stats.setDefaults(defConfig);
		}
	}

	// Bans
	public void saveBans() {
		if (banned == null || bannedFile == null) {
			return;
		}
		try {
			getBans().save(bannedFile);
		} catch (IOException ex) {
			this.getLogger().log(Level.SEVERE,
					"Could not save config to " + bannedFile, ex);
		}
	}

	public FileConfiguration getBans() {
		if (banned == null) {
			this.reloadBans();
		}
		return banned;
	}

	public void reloadBans() {
		if (bannedFile == null) {
			bannedFile = new File(getDataFolder(), "bans.yml");
		}
		banned = YamlConfiguration.loadConfiguration(bannedFile);
		InputStream defConfigStream = this.getResource("bans.yml");
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);
			banned.setDefaults(defConfig);
		}
	}

	// Maps
	public void saveMaps() {
		if (maps == null || mapsFile == null) {
			return;
		}
		try {
			getMaps().save(mapsFile);
		} catch (IOException ex) {
			this.getLogger().log(Level.SEVERE,
					"Could not save config to " + mapsFile, ex);
		}
	}

	public FileConfiguration getMaps() {
		if (maps == null) {
			this.reloadMaps();
		}
		return maps;
	}

	public void reloadMaps() {
		if (mapsFile == null) {
			mapsFile = new File(getDataFolder(), "maps.yml");
		}
		maps = YamlConfiguration.loadConfiguration(mapsFile);
		InputStream defConfigStream = this.getResource("maps.yml");
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration
					.loadConfiguration(defConfigStream);
			maps.setDefaults(defConfig);
		}
	}

	private void setupConfig(FileConfiguration config) throws IOException {
		if (!new File(getDataFolder(), "RESET.FILE").exists()) {
			config.set("Banned Words", "Fuck");
			config.set("Banned Words", "Shit");
			maps.set("Test", true);
			new File(getDataFolder(), "RESET.FILE").createNewFile();
		}
	}

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info("[" + pdfFile.getName() + "]" + " Has Been Disabled!");
		saveConfig();
		redTeamAmount = 0;
		blueTeamAmount = 0;
		purpleTeamAmount = 0;
		goldTeamAmount = 0;
		for (Player p : this.getServer().getOnlinePlayers()) {
			Arena1.remove(p);
			Arena2.remove(p);
			redTeam.remove(p);
			blueTeam.remove(p);
			purpleTeam.remove(p);
			goldTeam.remove(p);
			p.kickPlayer(ServerOffReason);
			this.getStats().set(
					"Data.Stats." + p.getName() + ".DeathsThisRound", 0);
		}

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (label.equalsIgnoreCase("warp")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (args.length == 0) {
					player.sendMessage(ChatColor.GOLD + "----------"
							+ ChatColor.AQUA + "Warps:" + ChatColor.GOLD
							+ "----------");
					player.sendMessage(this.getConfig().getString("Warps"));
				} else if (args.length == 1) {
					if (this.getConfig().contains("Warps." + args[0])) {
						int blockX = this.getConfig().getInt(
								"Warps." + args[0] + ".x");
						int blockY = this.getConfig().getInt(
								"Warps." + args[0] + ".y");
						int blockZ = this.getConfig().getInt(
								"Warps." + args[0] + ".z");
						String World = this.getConfig().getString(
								"Warps." + args[0] + ".world");
						Location warp = new Location(Bukkit.getWorld(World),
								blockX, blockY, blockZ);
						player.teleport(warp);
						player.sendMessage(message + ChatColor.GOLD
								+ "Warped to " + args[0] + "!");
					} else {
						player.sendMessage(message + ChatColor.DARK_RED
								+ "This warp does not exist!");
					}
				} else if (args.length == 2) {
					if (args[1].equalsIgnoreCase("set")) {
						if (player.hasPermission("yupball.warps.set")) {
							if (!(this.getConfig().contains("Warps." + args[0]))) {
								this.getConfig().set(
										"Warps." + args[0] + ".Exists", true);
								int blockX = player.getLocation().getBlockX();
								int blockY = player.getLocation().getBlockY();
								int blockZ = player.getLocation().getBlockZ();
								String world = player.getWorld().getName();
								this.getConfig().set("Warps." + args[0] + ".x",
										blockX);
								this.getConfig().set("Warps." + args[0] + ".y",
										blockY);
								this.getConfig().set("Warps." + args[0] + ".z",
										blockZ);
								this.getConfig().set(
										"Warps." + args[0] + ".world", world);
								player.sendMessage(message + ChatColor.GOLD
										+ "Warp " + args[0] + " set!");
								saveConfig();
							} else {
								player.sendMessage(message + ChatColor.DARK_RED
										+ "This warp already exists!");
							}
						} else {
							player.sendMessage(this.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[1].equalsIgnoreCase("remove")) {
						if (player.hasPermission("yupball.warps.remove")) {
							if (!(this.getConfig().contains("Warps." + args[0]))) {
								player.sendMessage(message + ChatColor.DARK_RED
										+ "This warp does not exist!");
							} else {
								this.getConfig()
										.getConfigurationSection(
												"Warps." + args[0])
										.set("", null);
								saveConfig();
								player.sendMessage(message + ChatColor.DARK_RED
										+ "The warp " + args[0]
										+ " was deleted!");
							}
						} else {
							player.sendMessage(this.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					}
				}
			}
		}
		return false;
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (this.getConfig().getBoolean("QuitMessages") == false) {
			event.setQuitMessage(null);
		}
		Player player = event.getPlayer();
		this.getStats().set(
				"Data.Stats." + player.getName() + ".DeathsThisRound", 0);
		saveStats();
		TabAPI.setPriority(this, player, -2);
		TabAPI.disableTabForPlayer(player);
		TabAPI.updatePlayer(player);
		Arena1.remove(player);
		Arena2.remove(player);
		if (redTeam.contains(player)) {
			redTeam.remove(player);
			redTeamAmount--;
		} else if (blueTeam.contains(player)) {
			blueTeam.remove(player);
			blueTeamAmount--;
		} else if (purpleTeam.contains(player)) {
			purpleTeam.remove(player);
			purpleTeamAmount--;
		} else if (goldTeam.contains(player)) {
			goldTeam.remove(player);
			goldTeamAmount--;
		}
		if (player.hasPermission("yupball.staff.admin")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + "Admin "
					+ player.getName() + " has left the game!");
			admins.remove(player.getName());
		} else if (player.hasPermission("yupball.staff.mod")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + "Mod " + player.getName()
					+ " has left the game!");
			mods.remove(player.getName());
		}
		Bukkit.getScheduler().cancelTask(PlayerJoin.PJ);
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		if (event.toWeatherState()) {
			event.setCancelled(true);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerKill(PlayerDeathEvent event) {
		event.setDeathMessage(null);
		final Player player = event.getEntity().getPlayer();
		Player killer = event.getEntity().getKiller();
		if (killer instanceof Player) {
			int deaths = (this.getStats().getInt("Data.Stats."
					+ player.getName() + ".DeathsThisRound")) + 1;
			this.getStats().set(
					"Data.Stats." + player.getName() + ".DeathsThisRound",
					deaths);
			int kills = (this.getStats().getInt("Data.Stats."
					+ killer.getName() + ".Kills")) + 1;
			this.getStats().set("Data.Stats." + killer.getName() + ".Kills",
					kills);
			int score = (this.getStats().getInt("Data.Stats."
					+ killer.getName() + ".Points")) + 1;
			this.getStats().set("Data.Stats." + killer.getName() + ".Score",
					score);
			saveStats();
			if (this.getStats().getInt(
					"Data.Stats." + player.getName() + ".DeathsThisRound") == 1) {
				player.sendMessage(message + ChatColor.DARK_RED + "You have "
						+ ChatColor.GREEN + "2" + ChatColor.DARK_RED
						+ " lives left!");
				event.setDeathMessage(ChatColor.BLUE + player.getName()
						+ ChatColor.RED + " was hit by " + ChatColor.GREEN
						+ killer.getName());
			} else if (this.getStats().getInt(
					"Data.Stats." + player.getName() + ".DeathsThisRound") == 2) {
				player.sendMessage(message + ChatColor.DARK_RED + "You have "
						+ ChatColor.GREEN + "1" + ChatColor.DARK_RED
						+ " life left!");
				event.setDeathMessage(ChatColor.BLUE + player.getName()
						+ ChatColor.RED + " was hit by " + ChatColor.GREEN
						+ killer.getName());
			} else if (this.getStats().getInt(
					"Data.Stats." + player.getName() + ".DeathsThisRound") == 3) {
				player.sendMessage(message + ChatColor.DARK_RED + "You have "
						+ ChatColor.GREEN + "0" + ChatColor.DARK_RED
						+ " lives left!");
				event.setDeathMessage(ChatColor.BLUE + player.getName()
						+ ChatColor.RED + " was killed by " + ChatColor.GREEN
						+ killer.getName());
				if (redTeam.contains(player)) {
					redTeamAmount--;
					Bukkit.dispatchCommand(player, "leave");
				} else if (blueTeam.contains(player)) {
					blueTeamAmount--;
					Bukkit.dispatchCommand(player, "leave");
				} else if (goldTeam.contains(player)) {
					goldTeamAmount--;
					Bukkit.dispatchCommand(player, "leave");
				} else if (purpleTeam.contains(player)) {
					purpleTeamAmount--;
					Bukkit.dispatchCommand(player, "leave");
				}
				if (redTeam.contains(player)) {
					if (redTeamAmount == 0) {
						Bukkit.broadcastMessage(message + ChatColor.GREEN
								+ "Blue Team won on Arena 1");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
								"forceend 1");
						for (Player p : redTeam) {
							int won = getStats().getInt(
									"Data.Stats." + p.getName()
											+ ".MatchesLost");
							getStats().set(
									"Data.Stats." + p.getName()
											+ ".MatchesLost", won + 1);
						}
						for (Player p : blueTeam) {
							int won = getStats()
									.getInt("Data.Stats." + p.getName()
											+ ".MatchesWon");
							getStats()
									.set("Data.Stats." + p.getName()
											+ ".MatchesWon", won + 1);
						}
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {
									public void run() {
										Bukkit.dispatchCommand(
												Bukkit.getConsoleSender(),
												"forcestart 1");
									}
								}, 1200L);
					}
				} else if (blueTeam.contains(player)) {
					if (blueTeamAmount == 0) {
						Bukkit.broadcastMessage(message + ChatColor.GREEN
								+ "Red Team won on Arena 1");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
								"forceend 1");
						for (Player p : blueTeam) {
							int won = getStats().getInt(
									"Data.Stats." + p.getName()
											+ ".MatchesLost");
							getStats().set(
									"Data.Stats." + p.getName()
											+ ".MatchesLost", won + 1);
						}
						for (Player p : redTeam) {
							int won = getStats()
									.getInt("Data.Stats." + p.getName()
											+ ".MatchesWon");
							getStats()
									.set("Data.Stats." + p.getName()
											+ ".MatchesWon", won + 1);
						}
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {
									public void run() {
										Bukkit.dispatchCommand(
												Bukkit.getConsoleSender(),
												"forcestart 1");
									}
								}, 1200L);
					}
				} else if (goldTeam.contains(player)) {
					if (goldTeamAmount == 0) {
						Bukkit.broadcastMessage(message + ChatColor.GREEN
								+ "Purple Team won on Arena 2");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
								"forceend 2");
						for (Player p : goldTeam) {
							int won = getStats().getInt(
									"Data.Stats." + p.getName()
											+ ".MatchesLost");
							getStats().set(
									"Data.Stats." + p.getName()
											+ ".MatchesLost", won + 1);
						}
						for (Player p : purpleTeam) {
							int won = getStats()
									.getInt("Data.Stats." + p.getName()
											+ ".MatchesWon");
							getStats()
									.set("Data.Stats." + p.getName()
											+ ".MatchesWon", won + 1);
						}
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {
									public void run() {
										Bukkit.dispatchCommand(
												Bukkit.getConsoleSender(),
												"forcestart 2");
									}
								}, 1200L);
					}
				} else if (purpleTeam.contains(player)) {
					if (goldTeamAmount == 0) {
						Bukkit.broadcastMessage(message + ChatColor.GREEN
								+ "Purple Team won on Arena 2");
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
								"forceend 2");
						for (Player p : purpleTeam) {
							int won = getStats().getInt(
									"Data.Stats." + p.getName()
											+ ".MatchesLost");
							getStats().set(
									"Data.Stats." + p.getName()
											+ ".MatchesLost", won + 1);
						}
						for (Player p : goldTeam) {
							int won = getStats()
									.getInt("Data.Stats." + p.getName()
											+ ".MatchesWon");
							getStats()
									.set("Data.Stats." + p.getName()
											+ ".MatchesWon", won + 1);
						}
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {
									public void run() {
										Bukkit.dispatchCommand(
												Bukkit.getConsoleSender(),
												"forcestart 2");
									}
								}, 1200L);
					}
				}
			} else if (this.getStats().getInt(
					"Data.Stats." + player.getName() + ".DeathsThisRound") == -1) {
				player.sendMessage(message + ChatColor.DARK_RED + "You have "
						+ ChatColor.GREEN + "3" + ChatColor.DARK_RED
						+ " lives left!");
				event.setDeathMessage(ChatColor.BLUE + player.getName()
						+ ChatColor.RED + " was hit by " + ChatColor.GREEN
						+ killer.getName());
			}
			if (killstreak.containsKey(killer.getName())) {
				int kills1 = killstreak.get(killer.getName());
				killstreak.put(killer.getName(), kills1 + 1);
				if (killstreak.get(killer.getName()) == 3) {
					Bukkit.broadcastMessage(message + ChatColor.GREEN
							+ killer.getName() + ChatColor.GOLD
							+ " is on a killstreak of 3. Someone stop them!");
					Player[] onlinePlayers = this.getServer()
							.getOnlinePlayers();
					for (Player players : onlinePlayers) {
						players.playSound(players.getLocation(),
								Sound.ENDERDRAGON_GROWL, 1F, 1F);
					}
					if (killer.hasPermission("yupball.donate.speedy")) {
						PotionEffect Speed = new PotionEffect(
								PotionEffectType.SPEED, 10000, 1);
						killer.addPotionEffect(Speed);
					} else if (killer.hasPermission("yupball.donate.heavy")) {
						int time = heavyDonatorTime.get(killer.getName());
						time = time + 5;
						heavyDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.doctor")) {
						int time = doctorDonatorTime.get(killer.getName());
						time = time - 5;
						doctorDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.ninja")) {
						ItemStack ender = new ItemStack(Material.ENDER_PEARL, 1);
						player.getInventory().addItem(ender);
					}
				} else if (killstreak.get(killer.getName()) == 5) {
					Bukkit.broadcastMessage(message + ChatColor.GREEN
							+ killer.getName() + ChatColor.GOLD
							+ " is on a killstreak of 5. Someone stop them!");
					Player[] onlinePlayers = this.getServer()
							.getOnlinePlayers();
					for (Player players : onlinePlayers) {
						players.playSound(players.getLocation(),
								Sound.ENDERDRAGON_GROWL, 1F, 1F);
					}
					if (killer.hasPermission("yupball.donate.speedy")) {
						PotionEffect Speed = new PotionEffect(
								PotionEffectType.SPEED, 10000, 2);
						killer.addPotionEffect(Speed);
					} else if (killer.hasPermission("yupball.donate.heavy")) {
						int time = heavyDonatorTime.get(killer.getName());
						time = time + 5;
						heavyDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.doctor")) {
						int time = doctorDonatorTime.get(killer.getName());
						time = time - 5;
						doctorDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.ninja")) {
						ItemStack ender = new ItemStack(Material.ENDER_PEARL, 1);
						player.getInventory().addItem(ender);
					}
				} else if (killstreak.get(killer.getName()) == 7) {
					Bukkit.broadcastMessage(message + ChatColor.GREEN
							+ killer.getName() + ChatColor.GOLD
							+ " is on a killstreak of 7. Someone stop them!");
					Player[] onlinePlayers = this.getServer()
							.getOnlinePlayers();
					for (Player players : onlinePlayers) {
						players.playSound(players.getLocation(),
								Sound.ENDERDRAGON_GROWL, 1F, 1F);
					}
					if (killer.hasPermission("yupball.donate.heavy")) {
						int time = heavyDonatorTime.get(killer.getName());
						time = time + 5;
					} else if (killer.hasPermission("yupball.donate.doctor")) {
						int time = doctorDonatorTime.get(killer.getName());
						time = time - 5;
						doctorDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.ninja")) {
						ItemStack ender = new ItemStack(Material.ENDER_PEARL, 1);
						player.getInventory().addItem(ender);
					}
				} else if (killstreak.get(killer.getName()) == 10) {
					Bukkit.broadcastMessage(message + ChatColor.GREEN
							+ killer.getName() + ChatColor.GOLD
							+ " is on a killstreak of 10. Someone stop them!");
					Player[] onlinePlayers = this.getServer()
							.getOnlinePlayers();
					for (Player players : onlinePlayers) {
						players.playSound(players.getLocation(),
								Sound.ENDERDRAGON_GROWL, 1F, 1F);
					}
					if (killer.hasPermission("yupball.speedy")) {
						PotionEffect Jump = new PotionEffect(
								PotionEffectType.JUMP, 200, 2);
						killer.addPotionEffect(Jump);
						killer.getInventory().addItem(
								new ItemStack(373, 5, (short) 16386));
						killer.updateInventory();
					} else if (killer.hasPermission("yupball.donate.heavy")) {
						int time = heavyDonatorTime.get(killer.getName());
						time = time + 5;
					} else if (killer.hasPermission("yupball.donate.doctor")) {
						int time = doctorDonatorTime.get(killer.getName());
						time = time - 5;
						doctorDonatorTime.put(killer.getName(), time);
					} else if (killer.hasPermission("yupball.donate.ninja")) {
						ItemStack ender = new ItemStack(Material.ENDER_PEARL, 1);
						player.getInventory().addItem(ender);
					}
				}
			}
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				Packet205ClientCommand packet = new Packet205ClientCommand();
				packet.a = 1;
				((CraftPlayer) player).getHandle().playerConnection.a(packet);
			}
		}, 0);
		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(this, new Runnable() {

					@Override
					public void run() {
						player.getLocation()
								.getWorld()
								.playEffect(player.getLocation(), Effect.SMOKE,
										5);

					}

				}, 10L);

	}

	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if (event.getCause() == DamageCause.FALL) {
			event.setCancelled(true);
		}
		if (event.getEntity() instanceof Player) {
			if (event.getEntity().getWorld().getName()
					.equalsIgnoreCase("spawn")) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlayerTag(PlayerReceiveNameTagEvent event) {
		Player player = event.getNamedPlayer();
		if (redTeam.contains(player)) {
			if (SpyDisguise.contains(player)) {
				event.setTag(ChatColor.BLUE + "BLUE");
			} else {
				event.setTag(ChatColor.DARK_RED + "RED");
			}
		} else if (blueTeam.contains(player)) {
			if (SpyDisguise.contains(player)) {
				event.setTag(ChatColor.DARK_RED + "RED");
			} else {
				event.setTag(ChatColor.BLUE + "BLUE");
			}
		} else {
			if (player.hasPermission("yupball.staff")) {
				event.setTag(ChatColor.GREEN + player.getName());
			} else if (player.getName().equalsIgnoreCase("yupie_123")) {
				event.setTag(ChatColor.DARK_PURPLE + player.getName());
			} else {
				event.setTag(ChatColor.DARK_GRAY + player.getName());
			}
		}
	}

	@EventHandler
	public void onSnowballHit(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Snowball) {
			Snowball snowball = (Snowball) event.getDamager();
			if (event.getEntity() instanceof Player) {
				Player damaged = (Player) event.getEntity();
				for (ItemStack i : damaged.getInventory().getArmorContents()) {
					i.setDurability((short) 0);
				}
				if (snowball.getShooter() instanceof Player) {
					Player shooter = (Player) snowball.getShooter();
					if (damaged.getItemInHand().getType() == Material.IRON_HOE
							&& HeavyDonatorCD.contains(damaged)) {
						event.setCancelled(true);
						shooter.sendMessage(message + ChatColor.RED
								+ "Your shot was deflected by A Heavy Donator!");
					} else if (redTeam.contains(shooter.getName())
							&& redTeam.contains(damaged.getName())) {
						if (!(snowball.getShooter() == damaged)) {
							shooter.sendMessage(message + ChatColor.DARK_RED
									+ "You hit one of your teammates");
							event.setCancelled(true);
							this.getStats().set(
									"Data.Stats." + shooter.getName()
											+ ".Points",
									(this.getStats().getInt(
											"Data.Stats." + shooter.getName()
													+ ".Points") - 1));
						}
					} else if (blueTeam.contains(shooter.getName())
							&& blueTeam.contains(damaged.getName())) {
						if (!(snowball.getShooter() == damaged)) {
							shooter.sendMessage(message + ChatColor.DARK_RED
									+ "You hit one of your teammates");
							event.setCancelled(true);
							this.getStats().set(
									"Data.Stats." + shooter.getName()
											+ ".Points",
									(this.getStats().getInt(
											"Data.Stats." + shooter.getName()
													+ ".Points") - 1));
						}
					} else if (purpleTeam.contains(shooter.getName())
							&& purpleTeam.contains(damaged.getName())) {
						if (!(snowball.getShooter() == damaged)) {
							shooter.sendMessage(message + ChatColor.DARK_RED
									+ "You hit one of your teammates");
							event.setCancelled(true);
							this.getStats().set(
									"Data.Stats." + shooter.getName()
											+ ".Points",
									(this.getStats().getInt(
											"Data.Stats." + shooter.getName()
													+ ".Points") - 1));
						}
					} else if (goldTeam.contains(shooter.getName())
							&& goldTeam.contains(damaged.getName())) {
						if (!(snowball.getShooter() == damaged)) {
							shooter.sendMessage(message + ChatColor.DARK_RED
									+ "You hit one of your teammates");
							event.setCancelled(true);
							this.getStats().set(
									"Data.Stats." + shooter.getName()
											+ ".Points",
									(this.getStats().getInt(
											"Data.Stats." + shooter.getName()
													+ ".Points") - 1));
						}
					} else {
						if (!(snowball.getShooter() == damaged)) {
							damaged.setHealth(0);
							shooter.sendMessage(message + ChatColor.GOLD
									+ "You hit " + ChatColor.RED
									+ damaged.getName() + ChatColor.RESET + "!");
						}
					}
				}
			}
		} else if (event.getDamager() instanceof Player) {
			if (event.getEntity() instanceof Player) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Block b = event.getBlockPlaced();
		if (b.getType().equals(Material.STRING)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onSignPlace(SignChangeEvent event) {
		if (event.getLine(0).equalsIgnoreCase("")) {
			if (event.getLine(1).equalsIgnoreCase("Spawn")) {
				event.setLine(1, ChatColor.UNDERLINE + "" + ChatColor.BOLD
						+ "Back to");
				event.setLine(2, ChatColor.UNDERLINE + "" + ChatColor.BOLD
						+ "Spawn");
			}
		} else if (event.getLine(0).equalsIgnoreCase("[YuPball]")) {
			if (event.getLine(2).equalsIgnoreCase("Arena 1")) {
				event.setLine(0, ChatColor.DARK_GRAY + "[" + ChatColor.GOLD
						+ "YuPball" + ChatColor.DARK_GRAY + "]");
			} else if (event.getLine(2).equalsIgnoreCase("Arena 2")) {
				event.setLine(0, ChatColor.DARK_GRAY + "[" + ChatColor.GOLD
						+ "YuPball" + ChatColor.DARK_GRAY + "]");
			} else if (event.getLine(1).equalsIgnoreCase("Arena 1")) {
				if (event.getLine(2).equalsIgnoreCase("Join")) {
					event.setLine(0, ChatColor.DARK_GRAY + "[" + ChatColor.GOLD
							+ "YuPball" + ChatColor.DARK_GRAY + "]");
					event.setLine(1, ChatColor.BOLD + "Arena 1");
					event.setLine(2, ChatColor.ITALIC + "Join");
				}
			} else if (event.getLine(1).equalsIgnoreCase("Arena 2")) {
				if (event.getLine(2).equalsIgnoreCase("Join")) {
					event.setLine(0, ChatColor.DARK_GRAY + "[" + ChatColor.GOLD
							+ "YuPball" + ChatColor.DARK_GRAY + "]");
					event.setLine(1, ChatColor.BOLD + "Arena 2");
					event.setLine(2, ChatColor.ITALIC + "Join");
				}
			}
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		if (event.getPlayer().getItemInHand().getType() == Material.DIAMOND_SWORD) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR
					|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				PotionEffect slowness6 = new PotionEffect(
						PotionEffectType.SLOW, 100000, 19);
				if (zoomedIn.get(player.getName()) == false) {
					player.addPotionEffect(slowness6);
					zoomedIn.put(player.getName(), true);
				} else if (zoomedIn.get(player.getName()) == true) {
					player.removePotionEffect(PotionEffectType.SLOW);
					zoomedIn.put(player.getName(), false);

				}
			} else if (event.getAction() == Action.LEFT_CLICK_AIR
					|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
				Block block = player.getTargetBlock(null, 50);
				final Material material = block.getType();
				final Location location = block.getLocation();
				final World world = block.getWorld();
				location.getBlock().setType(Material.TNT);
				Bukkit.getServer().getScheduler()
						.scheduleSyncDelayedTask(this, new Runnable() {
							@Override
							public void run() {
								world.createExplosion(location.getX(),
										location.getY(), location.getZ(), 3,
										false, false);
								location.getBlock().setType(material);
							}

						}, 200L);
			}
		} else if (player.getItemInHand().getType() == Material.IRON_SWORD) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				if (!DoctorDonatorTimes.containsKey(player.getName())) {
					if (doctorDonatorTime.containsKey(player.getName())) {
						final int time = doctorDonatorTime
								.get(player.getName());
						player.launchProjectile(Fireball.class);
						player.playSound(player.getLocation(),
								Sound.WITHER_SHOOT, 2F, 2F);
						DoctorDonatorCD.add(player);
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {

									@Override
									public void run() {
										DoctorDonatorTimes.put(
												player.getName(), 1);
										DoctorDonatorCD.remove(player);
									}

								}, (time * 20));
					} else {
						final int time = 25;
						doctorDonatorTime.put(player.getName(), 25);
						player.launchProjectile(Fireball.class);
						player.playSound(player.getLocation(),
								Sound.WITHER_SHOOT, 2F, 2F);
						DoctorDonatorCD.add(player);
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {

									@Override
									public void run() {
										DoctorDonatorTimes.put(
												player.getName(), 1);
										DoctorDonatorCD.remove(player);
									}

								}, (time * 20));
					}
				} else if (DoctorDonatorCD.contains(player)) {
					player.sendMessage(message + ChatColor.YELLOW
							+ "Please wait until thi item has cooled down!");
				} else if (DoctorDonatorTimes.containsKey(player.getName())) {
					if (DoctorDonatorTimes.get(player.getName()) == 1) {
						if (doctorDonatorTime.containsKey(player.getName())) {
							final int time = doctorDonatorTime.get(player
									.getName());
							player.launchProjectile(Fireball.class);
							player.playSound(player.getLocation(),
									Sound.WITHER_SHOOT, 2F, 2F);
							DoctorDonatorCD.add(player);
							Bukkit.getServer()
									.getScheduler()
									.scheduleSyncDelayedTask(this,
											new Runnable() {

												@Override
												public void run() {
													DoctorDonatorTimes.put(
															player.getName(), 1);
													DoctorDonatorCD
															.remove(player);
												}

											}, (time * 20));
						} else {
							final int time = 25;
							doctorDonatorTime.put(player.getName(), 25);
							player.launchProjectile(Fireball.class);
							player.playSound(player.getLocation(),
									Sound.WITHER_SHOOT, 2F, 2F);
							DoctorDonatorCD.add(player);
							Bukkit.getServer()
									.getScheduler()
									.scheduleSyncDelayedTask(this,
											new Runnable() {

												@Override
												public void run() {
													DoctorDonatorTimes.put(
															player.getName(), 1);
													DoctorDonatorCD
															.remove(player);
												}

											}, (time * 20));
						}
					} else {
						player.sendMessage(message + ChatColor.YELLOW
								+ "You may not use this again this round!");
					}
				}
			}
		} else if (player.getItemInHand().getType() == Material.BOOK) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				if (redTeam.contains(player)) {
					for (Player p : blueTeam) {
						p.hidePlayer(player);
					}
				} else if (blueTeam.contains(player)) {
					for (Player p : redTeam) {
						p.hidePlayer(player);
					}
				} else if (purpleTeam.contains(player)) {
					for (Player p : goldTeam) {
						p.hidePlayer(player);
					}
				} else if (goldTeam.contains(player)) {
					for (Player p : purpleTeam) {
						p.hidePlayer(player);
					}
				}
				player.sendMessage(message + ChatColor.GREEN
						+ "You are no longer visible to the other team!");
				Bukkit.getServer().getScheduler()
						.scheduleSyncDelayedTask(this, new Runnable() {

							@Override
							public void run() {
								for (Player p : Bukkit.getOnlinePlayers()) {
									p.showPlayer(player);
								}
								player.sendMessage(message + ChatColor.GREEN
										+ "You are now visible again!");
							}

						}, 200L);

			}
		} else if (event.getPlayer().getItemInHand().getType() == Material.STICK) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				player.launchProjectile(Fireball.class);
				player.playSound(player.getLocation(), Sound.WITHER_SHOOT, 2F,
						2F);
				player.getInventory().removeItem(
						new ItemStack(Material.STICK, 1));
			}
		} else if (event.getPlayer().getItemInHand().getType() == Material.FEATHER) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				int duration_seconds = 120;
				PotionEffect Speed = new PotionEffect(PotionEffectType.SPEED,
						(duration_seconds * 20), 1);
				player.addPotionEffect(Speed);
				ItemStack Speed1 = new ItemStack(Material.FEATHER, 1);
				ItemMeta Speed1M = Speed1.getItemMeta();
				Speed1M.setDisplayName(ChatColor.AQUA + "Speed (2:00)");
				List<String> Speed1S = new ArrayList<String>();
				Speed1S.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
						+ "Richt click to consume!");
				Speed1M.setLore(Speed1S);
				Speed1.setItemMeta(Speed1M);
				player.getInventory().removeItem(Speed1);
			}
		} else if (event.getPlayer().getItemInHand().getType() == Material.STRING) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				int duration_seconds = 120;
				PotionEffect Jump = new PotionEffect(PotionEffectType.JUMP,
						(duration_seconds * 20), 1);
				player.addPotionEffect(Jump);
				ItemStack Speed = new ItemStack(Material.STRING, 1);
				ItemMeta SpeedM = Speed.getItemMeta();
				SpeedM.setDisplayName(ChatColor.AQUA + "Jump (2:00)");
				List<String> SpeedS = new ArrayList<String>();
				SpeedS.add(ChatColor.DARK_GRAY + "" + ChatColor.ITALIC
						+ "Right click to consume!");
				SpeedM.setLore(SpeedS);
				Speed.setItemMeta(SpeedM);
				player.getInventory().removeItem(Speed);
			}
		} else if (event.getPlayer().getItemInHand().getType() == Material.GOLD_SWORD) {
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.RIGHT_CLICK_AIR) {
				if (HeavyDonatorDone.contains(player)) {
					player.sendMessage(message + ChatColor.YELLOW
							+ "You may not use this again this round!");
				} else {
					HeavyDonatorCD.add(player);
					if (heavyDonatorTime.containsKey(player.getName())) {
						int time = heavyDonatorTime.get(player.getName());
						player.getWorld().playEffect(player.getLocation(),
								Effect.MOBSPAWNER_FLAMES, 1);
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {

									@Override
									public void run() {
										HeavyDonatorCD.remove(player);
										HeavyDonatorDone.add(player);
									}

								}, (time * 20));

					} else {
						int time = 15;
						heavyDonatorTime.put(player.getName(), 15);
						player.getWorld().playEffect(player.getLocation(),
								Effect.MOBSPAWNER_FLAMES, 1);
						Bukkit.getServer().getScheduler()
								.scheduleSyncDelayedTask(this, new Runnable() {

									@Override
									public void run() {
										HeavyDonatorCD.remove(player);
										HeavyDonatorDone.add(player);
									}

								}, (time * 20));
					}

				}
			}
		} else if (event.getPlayer().getItemInHand().getType() == Material.ARROW) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR
					|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (SpyDisguise.contains(player)) {
					player.sendMessage(message + ChatColor.YELLOW
							+ "You cannot disguise yourself multiple times!");
				} else {
					player.sendMessage(message
							+ ChatColor.GREEN
							+ "You have been disguised as one of the other team!");
					SpyDisguise.add(player);
					Bukkit.getServer().getScheduler()
							.scheduleSyncDelayedTask(this, new Runnable() {

								@Override
								public void run() {
									SpyDisguise.remove(player);
								}

							}, (60 * 20));
				}
			}
		}
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block b = event.getClickedBlock();
			if (b.getType() == Material.SIGN
					|| b.getType() == Material.SIGN_POST
					|| b.getType() == Material.WALL_SIGN) {
				Sign s = (Sign) event.getClickedBlock().getState();
				String[] lines = s.getLines();
				if (lines[0].equalsIgnoreCase("")) {
					if (lines[1].equalsIgnoreCase(ChatColor.UNDERLINE + ""
							+ ChatColor.BOLD + "Back to")) {
						if (lines[2].equalsIgnoreCase(ChatColor.UNDERLINE + ""
								+ ChatColor.BOLD + "Spawn")) {
							double X = getConfig().getDouble("Spawn.X");
							double Y = getConfig().getDouble("Spawn.Y");
							double Z = getConfig().getDouble("Spawn.Z");
							String world = getConfig().getString("Spawn.World");
							float Yaw = getConfig().getInt("Spawn.Yaw");
							float Pitch = getConfig().getInt("Spawn.Pitch");
							Location spawn = new Location(getServer().getWorld(
									world), X, Y, Z, Yaw, Pitch);
							player.teleport(spawn);
							player.sendMessage(message
									+ ChatColor.GOLD
									+ "You were teleported to the Server spawn!");
						}
					}
				} else if (lines[0].equalsIgnoreCase(ChatColor.DARK_GRAY + "["
						+ ChatColor.GOLD + "YuPball" + ChatColor.DARK_GRAY
						+ "]")) {
					if (lines[1].equalsIgnoreCase(ChatColor.BOLD + "Arena 1")) {
						if (lines[2]
								.equalsIgnoreCase(ChatColor.ITALIC + "Join")) {
							if (redTeam.contains(player)
									|| blueTeam.contains(player)
									|| purpleTeam.contains(player)
									|| goldTeam.contains(player)) {
								player.sendMessage(message + ChatColor.DARK_RED
										+ "You cannot join a team again!");
							} else {
								if (InGame1 == true) {
									player.sendMessage(message
											+ ChatColor.DARK_RED
											+ "You cannot join a game in progress, Sorry!");
								} else {
									Bukkit.getServer().dispatchCommand(player,
											"join");
								}
							}
						}
					} else if (lines[1].equalsIgnoreCase(ChatColor.BOLD
							+ "Arena 2")) {
						if (lines[2]
								.equalsIgnoreCase(ChatColor.ITALIC + "Join")) {
							if (redTeam.contains(player)
									|| blueTeam.contains(player)
									|| purpleTeam.contains(player)
									|| goldTeam.contains(player)) {
								player.sendMessage(message + ChatColor.DARK_RED
										+ "You cannot join a team again!");
							} else {
								if (InGame2 == true) {
									player.sendMessage(message
											+ ChatColor.DARK_RED
											+ "You cannot join a game in progress, Sorry!");
								} else {
									Bukkit.getServer().dispatchCommand(player,
											"join");
								}
							}
						}
					}
					if (lines[2].equalsIgnoreCase("Arena 1")) {
						if (this.getConfig().contains("Arena.Arena 1.Lobby")) {
							String world = this.getConfig().getString(
									"Arena.Arena 1.Lobby.World");
							Double X = this.getConfig().getDouble(
									"Arena.Arena 1.Lobby.X");
							Double Y = this.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Y");
							Double Z = this.getConfig().getDouble(
									"Arena.Arena 1.Lobby.Z");
							float Yaw = this.getConfig().getInt(
									"Arena.Arena 1.Lobby.Yaw");
							float Pitch = this.getConfig().getInt(
									"Arena.Arena 1.Lobby.Pitch");
							Location lobby = new Location(getServer().getWorld(
									world), X, Y, Z, Yaw, Pitch);
							Bukkit.getServer().createWorld(
									new WorldCreator(world));
							player.teleport(lobby);
							player.sendMessage(message + ChatColor.GOLD
									+ "You were teleported to Arena 1's Lobby");
							Bukkit.getServer().dispatchCommand(player,
									"arena join 1");
						} else {
							player.sendMessage(message + ChatColor.DARK_RED
									+ "That lobby has not been created yet!");
						}
					} else if (lines[2].equalsIgnoreCase("Arena 2")) {
						if (this.getConfig().contains("Arena.Arena 2.Lobby")) {
							String world = this.getConfig().getString(
									"Arena.Arena 2.Lobby.World");
							Double X = this.getConfig().getDouble(
									"Arena.Arena 2.Lobby.X");
							Double Y = this.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Y");
							Double Z = this.getConfig().getDouble(
									"Arena.Arena 2.Lobby.Z");
							float Yaw = this.getConfig().getInt(
									"Arena.Arena 2.Lobby.Yaw");
							float Pitch = this.getConfig().getInt(
									"Arena.Arena 2.Lobby.Pitch");
							Location lobby = new Location(getServer().getWorld(
									world), X, Y, Z, Yaw, Pitch);
							Bukkit.getServer().createWorld(
									new WorldCreator(world));
							player.teleport(lobby);
							player.sendMessage(message + ChatColor.GOLD
									+ "You were teleported to Arena 2's Lobby");
							Bukkit.getServer().dispatchCommand(player,
									"arena join 2");
						} else {
							player.sendMessage(message + ChatColor.DARK_RED
									+ "That lobby has not been created yet!");
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onRedstoneEvent(BlockRedstoneEvent event) {
		Block block = event.getBlock();
		if (block.getType() == Material.STONE_PLATE) {
			Double blockX = block.getLocation().getX();
			Double blockY = block.getLocation().getY();
			Double blockZ = block.getLocation().getZ();
			World world = block.getWorld();
			world.createExplosion(blockX, blockY, blockZ, 10F, false, false);
			block.breakNaturally();

		}
	}

	@EventHandler
	public void dropSlimeball(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		World world = player.getWorld();
		if (player.getItemInHand().getType() == Material.SLIME_BALL) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR
					|| event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				player.getInventory().removeItem(Slimeball);
				final Item Slime = world.dropItem(player.getEyeLocation(),
						Slimeball);
				Slime.setVelocity(player.getLocation().getDirection()
						.normalize().multiply(0.4));
				Slime.setFallDistance(1);
				Bukkit.getScheduler().scheduleSyncDelayedTask(this,
						new Runnable() {
							@SuppressWarnings({ "deprecation", "unused" })
							@Override
							public void run() {
								if (Slime.getLocation().getBlock().getType() == Material.WEB) {
									if (Slime.getLocation().subtract(0, 2, 0)
											.getBlock().getType() == Material.EMERALD_ORE) {
										if (redTeam.contains(player)
												|| purpleTeam.contains(player)) {
											player.sendMessage(message
													+ ChatColor.GREEN
													+ "You scored well done!");
											if (redTeam.contains(player)) {
												Goals1red++;
												for (Player p : Arena1) {
													p.sendMessage(message
															+ ChatColor.GREEN
															+ player.getName()
															+ " has scored a goal for the Red Team!");
												}
											} else if (goldTeam
													.contains(player)) {
												Goals2gold++;
												for (Player p : Arena1) {
													p.sendMessage(message
															+ ChatColor.GREEN
															+ player.getName()
															+ " has scored a goal for the Gold Team!");
												}
											}
											int score = getStats().getInt(
													"Data.Stats."
															+ player.getName()
															+ ".Points") + 3;
											getStats().set(
													"Data.Stats."
															+ player.getName()
															+ ".Points", score);
											int goals = getStats().getInt(
													"Data.Stats."
															+ player.getName()
															+ ".Goals") + 1;
											getStats().set(
													"Data.Stats."
															+ player.getName()
															+ ".Goals", goals);
											saveStats();
											for (Entity e : Slime
													.getNearbyEntities(7, 7, 7)) {
												if (e instanceof Player) {
													Player p = (Player) e;
													Slime.getWorld()
															.createExplosion(
																	Slime.getLocation()
																			.getX(),
																	Slime.getLocation()
																			.getY(),
																	Slime.getLocation()
																			.getZ(),
																	7F, false,
																	false);
												}
											}
											Slime.remove();
											if (Goals1red == 3
													|| Goals2gold == 3) {
												if (Goals1red == 3) {
													Goals1red = 0;
													Goals1blue = 0;
													Bukkit.broadcastMessage(message
															+ ChatColor.GREEN
															+ "Red team have won on Arena 1");
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forceend 1");
													for (Player p : blueTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost",
																		won + 1);
													}
													for (Player p : redTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon",
																		won + 1);
													}
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forcestart 1");
												} else if (Goals2gold == 3) {
													Goals2gold = 0;
													Goals2purple = 0;
													Bukkit.broadcastMessage(message
															+ ChatColor.GREEN
															+ "Gold team have won on Arena 2");
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forceend 2");
													for (Player p : purpleTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost",
																		won + 1);
													}
													for (Player p : goldTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon",
																		won + 1);
													}
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forcestart 2");
												}
											}
										} else if (blueTeam.contains(player)
												|| purpleTeam.contains(player)) {
											player.sendMessage(message
													+ ChatColor.RED
													+ "You scored an own goal!");
											Slime.setTicksLived(6000);
											player.getInventory()
													.addItem(
															new ItemStack(
																	Material.SLIME_BALL,
																	1));
											player.updateInventory();
										}
									} else if (Slime.getLocation()
											.subtract(0, 2, 0).getBlock()
											.getType() == Material.DIAMOND_ORE) {
										if (blueTeam.contains(player)
												|| purpleTeam.contains(player)) {
											player.sendMessage(message
													+ ChatColor.GREEN
													+ "You scored well done!");
											if (blueTeam.contains(player)) {
												Goals1blue++;
												for (Player p : Arena1) {
													p.sendMessage(message
															+ ChatColor.GREEN
															+ player.getName()
															+ " has scored a goal for the Blue Team!");
												}
											} else if (purpleTeam
													.contains(player)) {
												Goals2purple++;
												for (Player p : Arena1) {
													p.sendMessage(message
															+ ChatColor.GREEN
															+ player.getName()
															+ " has scored a goal for the Purple Team!");
												}
											}
											int score = getStats().getInt(
													"Data.Stats."
															+ player.getName()
															+ ".Points") + 3;
											getStats().set(
													"Data.Stats."
															+ player.getName()
															+ ".Points", score);
											int goals = getStats().getInt(
													"Data.Stats."
															+ player.getName()
															+ ".Goals") + 1;
											getStats().set(
													"Data.Stats."
															+ player.getName()
															+ ".Goals", goals);
											saveStats();
											for (Entity e : Slime
													.getNearbyEntities(7, 7, 7)) {
												if (e instanceof Player) {
													Player p = (Player) e;
													Slime.getWorld()
															.createExplosion(
																	Slime.getLocation()
																			.getX(),
																	Slime.getLocation()
																			.getY(),
																	Slime.getLocation()
																			.getZ(),
																	7F, false,
																	false);
												}
											}
											Slime.remove();
											if (Goals1blue == 3
													|| Goals2purple == 3) {
												if (Goals1blue == 3) {
													Goals1red = 0;
													Goals1blue = 0;
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forceend 1");
													for (Player p : blueTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost",
																		won + 1);
													}
													for (Player p : redTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon",
																		won + 1);
													}
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forcestart 1");
												} else if (Goals2purple == 3) {
													Bukkit.getServer()
															.dispatchCommand(
																	Bukkit.getConsoleSender(),
																	"forceend 2");
													Goals2gold = 0;
													Goals2purple = 0;
													for (Player p : goldTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesLost",
																		won + 1);
													}
													for (Player p : purpleTeam) {
														int won = getStats()
																.getInt("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon");
														getStats()
																.set("Data.Stats."
																		+ p.getName()
																		+ ".MatchesWon",
																		won + 1);
													}
													Bukkit.dispatchCommand(
															Bukkit.getConsoleSender(),
															"forcestart 2");
												}
											}
										} else if (redTeam.contains(player)
												|| purpleTeam.contains(player)) {
											player.sendMessage(message
													+ ChatColor.RED
													+ "You scored an own goal!");
											Slime.setTicksLived(-1);
											player.getInventory()
													.addItem(
															new ItemStack(
																	Material.SLIME_BALL,
																	1));
											player.updateInventory();
										}
									}
									for (Entity e : Slime.getNearbyEntities(7,
											7, 7)) {
										if (e instanceof Player) {
											if (redTeam.contains(e)) {
												int mapID = getConfig().getInt(
														"Arena 1 MapID");
												String World = getMaps()
														.getString(
																"Map"
																		+ mapID
																		+ ".Team1.World");
												double X = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.X");
												double Y = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.Y");
												double Z = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.Z");
												float Yaw = getMaps().getInt(
														"Map" + mapID
																+ ".Team1.Yaw");
												float Pitch = getMaps()
														.getInt("Map"
																+ mapID
																+ ".Team1.Pitch");
												Location location = new Location(
														getServer().getWorld(
																World), X, Y,
														Z, Yaw, Pitch);
												e.teleport(location);
											} else if (blueTeam.contains(e)) {
												int mapID = getConfig().getInt(
														"Arena 1 MapID");
												String World = getMaps()
														.getString(
																"Map"
																		+ mapID
																		+ ".Team2.World");
												double X = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.X");
												double Y = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.Y");
												double Z = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.Z");
												float Yaw = getMaps().getInt(
														"Map" + mapID
																+ ".Team2.Yaw");
												float Pitch = getMaps()
														.getInt("Map"
																+ mapID
																+ ".Team2.Pitch");
												Location location = new Location(
														getServer().getWorld(
																World), X, Y,
														Z, Yaw, Pitch);
												e.teleport(location);
											} else if (purpleTeam.contains(e)) {
												int mapID = getConfig().getInt(
														"Arena 2 MapID");
												String World = getMaps()
														.getString(
																"Map"
																		+ mapID
																		+ ".Team1.World");
												double X = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.X");
												double Y = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.Y");
												double Z = getMaps().getDouble(
														"Map" + mapID
																+ ".Team1.Z");
												float Yaw = getMaps().getInt(
														"Map" + mapID
																+ ".Team1.Yaw");
												float Pitch = getMaps()
														.getInt("Map"
																+ mapID
																+ ".Team1.Pitch");
												Location location = new Location(
														getServer().getWorld(
																World), X, Y,
														Z, Yaw, Pitch);
												e.teleport(location);
											} else if (goldTeam.contains(e)) {
												int mapID = getConfig().getInt(
														"Arena 2 MapID");
												String World = getMaps()
														.getString(
																"Map"
																		+ mapID
																		+ ".Team2.World");
												double X = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.X");
												double Y = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.Y");
												double Z = getMaps().getDouble(
														"Map" + mapID
																+ ".Team2.Z");
												float Yaw = getMaps().getInt(
														"Map" + mapID
																+ ".Team2.Yaw");
												float Pitch = getMaps()
														.getInt("Map"
																+ mapID
																+ ".Team2.Pitch");
												Location location = new Location(
														getServer().getWorld(
																World), X, Y,
														Z, Yaw, Pitch);
												e.teleport(location);
											}
										}
									}
								}
							}

						}, 20L);
			}
		}
	}

	@EventHandler
	public void EggThrow(PlayerEggThrowEvent event) {
		event.setHatching(false);
	}

	@EventHandler
	public void EntityHit(ProjectileHitEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof Egg) {
			Double bX = entity.getLocation().getX();
			Double bY = entity.getLocation().getY();
			Double bZ = entity.getLocation().getZ();
			World world = entity.getWorld();
			world.createExplosion(bX, bY, bZ, 3F, false, false);
		}
	}

	@EventHandler
	public void EntityExplode(EntityExplodeEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof Fireball) {
			Entity xp = event
					.getLocation()
					.getWorld()
					.spawnEntity(event.getLocation(), EntityType.EXPERIENCE_ORB);
			List<Entity> lEnt = xp.getNearbyEntities(3, 3, 3);
			for (Entity e : lEnt) {
				if (e instanceof Player) {
					Player player = (Player) e;
					player.setHealth(0);
				}
			}
			event.setCancelled(true);
			Double bX = entity.getLocation().getX();
			Double bY = entity.getLocation().getY();
			Double bZ = entity.getLocation().getZ();
			World world = entity.getWorld();
			world.createExplosion(bX, bY, bZ, 10F, false, false);
		}
	}

	/*
	 * @EventHandler public void onBlockBreak(BlockBreakEvent event) { Player p
	 * = event.getPlayer(); if (!p.hasPermission("yupball.map.edit")) {
	 * event.setCancelled(true); }
	 * 
	 * }
	 */

	@SuppressWarnings("deprecation")
	public void Trainee(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.fromRGB(99, 99, 99));
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();

	}

	@SuppressWarnings("deprecation")
	public void Amateur(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.YELLOW);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Advanced(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.GREEN);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Forge(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.fromRGB(26, 32, 65));
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Brigadier(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.GREEN);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Commander(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 0);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.PURPLE);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Minister(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 0);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.ORANGE);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Prime(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 0);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.RED);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void King(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 0);
		ItemStack LGB = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB.getItemMeta();
		LGBM.setColor(Color.FUCHSIA);
		LGB.setItemMeta(LGBM);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(LGB);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Lord(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 4);
		ItemStack boots = new ItemStack(Material.IRON_BOOTS, 1);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(boots);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Legate(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 4);
		ItemStack boots = new ItemStack(Material.GOLD_BOOTS, 1);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(boots);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Master(Player name) {
		Player player = (Player) name;
		player.getInventory().setArmorContents(null);
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 1);
		ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
		player.getInventory().setHelmet(head);
		player.getInventory().setBoots(boots);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Mod(Player name) {
		Player player = (Player) name;
		player.getInventory().setBoots(
				new ItemStack(Material.CHAINMAIL_BOOTS, 1));
		player.getInventory().setLeggings(
				new ItemStack(Material.CHAINMAIL_LEGGINGS, 1));
		player.getInventory().setChestplate(
				new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
		player.getInventory().setHelmet(
				new ItemStack(Material.CHAINMAIL_HELMET, 1));
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Admin(Player name) {
		Player player = (Player) name;
		ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS, 1);
		ItemStack legs = new ItemStack(Material.CHAINMAIL_LEGGINGS, 1);
		ItemStack chest = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
		ItemStack head = new ItemStack(Material.CHAINMAIL_HELMET, 1);
		player.getInventory().setBoots(boots);
		player.getInventory().setLeggings(legs);
		player.getInventory().setChestplate(chest);
		player.getInventory().setHelmet(head);
		player.getInventory().getBoots()
				.addEnchantment(Enchantment.DURABILITY, 1);
		player.getInventory().getLeggings()
				.addEnchantment(Enchantment.DURABILITY, 1);
		player.getInventory().getChestplate()
				.addEnchantment(Enchantment.DURABILITY, 1);
		player.getInventory().getHelmet()
				.addEnchantment(Enchantment.DURABILITY, 1);
		ItemStack snowballs = new ItemStack(Material.SNOW_BALL, 50);
		ItemStack slime = new ItemStack(Material.SLIME_BALL, 3);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(slime);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Speedy(Player name) {
		Player player = (Player) name;
		player.getInventory().addItem(new ItemStack(373, 5, (short) 16386));
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void tempSpeedy(Player name) {
		Player player = (Player) name;
		player.getInventory().addItem(new ItemStack(373, 5, (short) 16386));
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Soldier(Player name) {
		Player player = (Player) name;
		player.getInventory().setMaxStackSize(100);
		ItemStack snowballs = new ItemStack(332, 64);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(snowballs);
		player.getInventory().addItem(new ItemStack(373, 5, (short) 16386));
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Heavy(Player name) {
		Player player = (Player) name;
		ItemStack gold_hoe = new ItemStack(Material.GOLD_SWORD, 1);
		player.getInventory().addItem(gold_hoe);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Doctor(Player name) {
		Player player = (Player) name;
		ItemStack IronSword = new ItemStack(Material.IRON_SWORD, 1);
		player.getInventory().addItem(IronSword);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Spy(Player name) {
		Player player = (Player) name;
		ItemStack book = new ItemStack(Material.BOOK, 1);
		player.getInventory().addItem(book);
		ItemStack arrow = new ItemStack(Material.ARROW, 1);
		player.getInventory().addItem(arrow);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Sniper(Player name) {
		Player player = (Player) name;
		ItemStack diaSword = new ItemStack(Material.DIAMOND_SWORD, 1);
		player.getInventory().addItem(diaSword);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Bomberman(Player name) {
		Player player = (Player) name;
		ItemStack eggs = new ItemStack(Material.EGG, 5);
		player.getInventory().addItem(eggs);
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public void Ninja(Player name) {
		Player player = (Player) name;
		ItemStack enderP = new ItemStack(Material.ENDER_PEARL, 2);
		player.getInventory().addItem(enderP);
		player.updateInventory();
	}

	@SuppressWarnings("unused")
	public void updateTabList(Player p) { // update the tab for a player
		int Score = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".Points");
		int Kills = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".Kills");
		int Deaths = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".Deaths");
		int Goals = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".Goals");
		int MatchesWon = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".MatchesWon");
		int MatchesLost = this.getStats()
				.getInt("Data.Stats." + ".MatchesLost");
		String rank = this.getStats().getString(
				"Data.Stats." + p.getName() + ".Rank");
		int Friends = this.getStats().getInt(
				"Data.Stats." + p.getName() + ".Friends.Amount");
		TabAPI.clearTab(p);
		String name = p.getName();
		TabAPI.setTabString(this, p, 0, 0, ChatColor.GOLD + " ------------ ",
				1000);
		TabAPI.setTabString(
				this,
				p,
				0,
				1,
				ChatColor.GOLD + ChatColor.BOLD.toString() + "  ------------- ",
				1000);
		TabAPI.setTabString(this, p, 0, 2, ChatColor.GOLD + " ------------- ",
				1000);
		TabAPI.setTabString(this, p, 1, 0,
				ChatColor.GREEN + ChatColor.BOLD.toString() + "    YuPCraft",
				1000);
		TabAPI.setTabString(this, p, 1, 1, ChatColor.DARK_GREEN
				+ ChatColor.BOLD.toString() + "     YuPball", 1000);
		TabAPI.setTabString(this, p, 1, 2,
				ChatColor.GREEN + ChatColor.BOLD.toString() + "    Server",
				1000);
		TabAPI.setTabString(this, p, 2, 0,
				ChatColor.GOLD + ChatColor.BOLD.toString() + " ------------ ",
				1000);
		TabAPI.setTabString(this, p, 2, 1, ChatColor.GOLD + "  ------------- "
				+ ChatColor.BOLD.toString(), 1000);
		TabAPI.setTabString(this, p, 2, 2,
				ChatColor.GOLD + ChatColor.BOLD.toString() + "------------- ",
				1000);
		TabAPI.setTabString(this, p, 3, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Score: ", -1);
		TabAPI.setTabString(this, p, 3, 1, ChatColor.WHITE + "" + Score, 1000);
		TabAPI.setTabString(this, p, 4, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Kills: ", -1);
		TabAPI.setTabString(this, p, 4, 1, ChatColor.WHITE + "" + Kills + " ",
				1000);
		TabAPI.setTabString(this, p, 5, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Deaths: ", -1);
		TabAPI.setTabString(this, p, 5, 1,
				ChatColor.WHITE + "" + Deaths + "  ", 1000);
		TabAPI.setTabString(this, p, 6, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Goals: ", -1);
		TabAPI.setTabString(this, p, 6, 1,
				ChatColor.WHITE + "" + Goals + "   ", 1000);
		TabAPI.setTabString(this, p, 7, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Matches Won: ",
				1000);
		TabAPI.setTabString(this, p, 7, 1, ChatColor.WHITE + "" + MatchesWon
				+ "    ", 1000);
		TabAPI.setTabString(this, p, 8, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Matches Lost: ",
				1000);
		TabAPI.setTabString(this, p, 8, 1, ChatColor.WHITE + "" + MatchesLost
				+ "     ", 1000);
		TabAPI.setTabString(this, p, 9, 0,
				ChatColor.BLUE + ChatColor.BOLD.toString() + "Rank: ", 1000);
		TabAPI.setTabString(this, p, 9, 1, ChatColor.WHITE + rank, 1000);
		// TabAPI.setTabString(this, p, 10, 0,
		// ChatColor.BLUE + ChatColor.BOLD.toString() + "Friends:", 1000);
		// TabAPI.setTabString(this, p, 10, 1, ChatColor.WHITE + "" + Friends
		// + "      ", 1000);
		// TabAPI.setTabString(this, p, 0, 0, ChatColor.DARK_PURPLE + "Stats: ",
		// -1);
		// if (name.length() >= 12) {
		// int difference = name.length() - 11;
		// name = name.substring(0, name.length() - difference);
		// TabAPI.setTabString(this, p, 0, 1, ChatColor.LIGHT_PURPLE + name,
		// -1);
		// }
		TabAPI.updatePlayer(p);
	}

	public boolean isInRect(Entity player, Location loc1, Location loc2) {
		double[] dim = new double[2];

		dim[0] = loc1.getX();
		dim[1] = loc2.getX();
		Arrays.sort(dim);
		if (player.getLocation().getX() > dim[1]
				|| player.getLocation().getX() < dim[0])
			return false;

		dim[0] = loc1.getZ();
		dim[1] = loc2.getZ();
		Arrays.sort(dim);
		if (player.getLocation().getZ() > dim[1]
				|| player.getLocation().getZ() < dim[0])
			return false;

		dim[0] = loc1.getY();
		dim[1] = loc2.getY();
		Arrays.sort(dim);
		if (player.getLocation().getY() > dim[1]
				|| player.getLocation().getY() < dim[0])
			return false;

		return true;
	}

	public int getRandom(int min, int max) {
		return rand.nextInt((max - min) + min);
	}

	@EventHandler
	public void onMobSpawn(CreatureSpawnEvent event) {
		event.setCancelled(true);
	}
}
