package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class DonatorAdd implements CommandExecutor {

	private YuPball plugin;

	public DonatorAdd(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender p, Command command, String label,
			String[] args) {
		if (p.hasPermission("yupball.donator")) {
			if (args.length != 3) {
				p.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /donator add <Player> <Rank>");
			} else {
				Player t = Bukkit.getServer().getPlayer(args[1]);
				if (t != null) {
					if (args[0].equalsIgnoreCase("add")) {
						if (args[2].equalsIgnoreCase("speedy")
								|| args[2].equalsIgnoreCase("soldier")
								|| args[2].equalsIgnoreCase("heavy")
								|| args[2].equalsIgnoreCase("doctor")
								|| args[2].equalsIgnoreCase("spy")
								|| args[2].equalsIgnoreCase("sniper")
								|| args[2].equalsIgnoreCase("bomberman")
								|| args[2].equalsIgnoreCase("ninja")
								|| args[2].equalsIgnoreCase("supporter")
								|| args[2].equalsIgnoreCase("premium")
								|| args[2].equalsIgnoreCase("precioous")
								|| args[2].equalsIgnoreCase("preciousplus")) {
							plugin.getStats().set(
									"Data.Stats." + t.getName() + ".Donator",
									args[2]);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "You set " + args[1]
									+ " to donator rank " + args[2]);
							plugin.saveStats();
						} else {
							p.sendMessage(plugin.message + ChatColor.DARK_RED
									+ "That rank does not exist!");
						}
					} else if (args[0].equalsIgnoreCase("remove")) {
						if (args[2].equalsIgnoreCase("speedy")
								|| args[2].equalsIgnoreCase("soldier")
								|| args[2].equalsIgnoreCase("heavy")
								|| args[2].equalsIgnoreCase("doctor")
								|| args[2].equalsIgnoreCase("spy")
								|| args[2].equalsIgnoreCase("sniper")
								|| args[2].equalsIgnoreCase("bomberman")
								|| args[2].equalsIgnoreCase("ninja")
								|| args[2].equalsIgnoreCase("supporter")
								|| args[2].equalsIgnoreCase("premium")
								|| args[2].equalsIgnoreCase("precioous")
								|| args[2].equalsIgnoreCase("preciousplus")) {
							plugin.getStats().set(
									"Data.Stats." + t.getName() + ".Donator",
									null);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "You removed " + args[1]
									+ "from donator rank " + args[2]);
							plugin.saveStats();
						} else {
							p.sendMessage(plugin.message + ChatColor.DARK_RED
									+ "That rank does not exist!");
						}
					}
				} else {
					p.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "That player is not online!");
					return true;
				}
			}
		} else {
			p.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to perform this command!");
			return true;
		}
		return false;
	}
}
