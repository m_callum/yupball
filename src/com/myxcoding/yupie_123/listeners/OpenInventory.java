package com.myxcoding.yupie_123.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;

import com.myxcoding.yupie_123.YuPball;

public class OpenInventory implements Listener {

	private YuPball plugin;

	public OpenInventory(YuPball plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		if (event.getInventory().getType() == InventoryType.BEACON
				|| event.getInventory().getType() == InventoryType.FURNACE
				|| event.getInventory().getType() == InventoryType.CRAFTING
				|| event.getInventory().getType() == InventoryType.ENCHANTING
				|| event.getInventory().getType() == InventoryType.CHEST
				|| event.getInventory().getType() == InventoryType.BREWING) {
			event.setCancelled(true);
		}
		if (event.getInventory().getName().equalsIgnoreCase("Shop")) {
			if (event.getPlayer().getWorld().getName()
					.equalsIgnoreCase("Spawn")) {
				event.setCancelled(true);
				Player player = (Player) event.getPlayer();
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "The shop can only be opened ingame!");
			}
			event.setCancelled(false);
		}
	}

}
