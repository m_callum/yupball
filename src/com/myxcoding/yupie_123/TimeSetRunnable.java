package com.myxcoding.yupie_123;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

public class TimeSetRunnable extends BukkitRunnable {

	@Override
	public void run() {
		for (World w : Bukkit.getServer().getWorlds()) {
			w.setTime(6000L);
		}

	}

}
