package com.myxcoding.yupie_123.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.kitteh.tag.TagAPI;
import org.mcsg.double0negative.tabapi.TabAPI;

import com.myxcoding.yupie_123.YuPball;

public class PlayerJoin implements Listener {

	private YuPball plugin;
	public int PJ;

	public PlayerJoin(YuPball plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().getInventory().setMaxStackSize(100);
		plugin.NormalChat.add(event.getPlayer());
		if (plugin.getConfig().getBoolean("JoinMessages") == false) {
			event.setJoinMessage(null);
		}
		String name = event.getPlayer().getName();
		if (plugin.getBans().getInt(name + ".Amount") == 5) {
			event.getPlayer().kickPlayer(
					ChatColor.DARK_RED
							+ "You have been banned from this server!");
		}
		final Player player = event.getPlayer();
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().clear();
		player.updateInventory();
		ItemStack Emerald = new ItemStack(Material.EMERALD, 1);
		ItemMeta EmeraldM = Emerald.getItemMeta();
		EmeraldM.setDisplayName(ChatColor.GREEN + "Shop");
		List<String> EmeraldS = new ArrayList<String>();
		EmeraldS.add(ChatColor.DARK_GRAY + ChatColor.ITALIC.toString()
				+ "Click to open Shop");
		EmeraldM.setLore(EmeraldS);
		Emerald.setItemMeta(EmeraldM);
		player.getInventory().setItem(17, Emerald);
		TabAPI.setPriority(plugin, player, -2);
		plugin.zoomedIn.put(player.getName(), false);
		if (plugin.getStats().contains("Data.Stats." + player.getName())) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
					new Runnable() {
						public void run() {
							plugin.updateTabList(player);
						}
					}, 3);
		} else {
			plugin.getStats().set("Data.Stats." + player.getName() + ".Points",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Kills",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Deaths",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Goals",
					0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".MatchesWon", 0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".MatchesLost", 0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".DeathsThisRound", 0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".KillsSinceDeath", 0);
			PJ = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
					new Runnable() {
						public void run() {
							plugin.updateTabList(player);
						}
					}, 3);
		}
		double X = plugin.getConfig().getDouble("Spawn.X");
		double Y = plugin.getConfig().getDouble("Spawn.Y");
		double Z = plugin.getConfig().getDouble("Spawn.Z");
		String world = plugin.getConfig().getString("Spawn.World");
		float Yaw = plugin.getConfig().getInt("Spawn.Yaw");
		float Pitch = plugin.getConfig().getInt("Spawn.Pitch");
		if (plugin.getConfig().contains("Spawn")) {
			Location spawn = new Location(plugin.getServer().getWorld(world),
					X, Y, Z, Yaw, Pitch);
			player.teleport(spawn);
		} else {
			player.teleport(player.getWorld().getSpawnLocation());
		}
		if (player.hasPlayedBefore() == true) {

			if (player.getName().equalsIgnoreCase("yupie_123")) {
				player.sendMessage(ChatColor.GOLD + "Welcome back "
						+ ChatColor.GREEN + player.getName());
				plugin.staff.add(name);
				Bukkit.broadcastMessage(ChatColor.GOLD
						+ "The Server owner has joined the game!");
				TagAPI.refreshPlayer(player);
			} else if (player.hasPermission("yupball.staff.admin")
					|| player.isOp() == true) {
				player.sendMessage(ChatColor.GOLD + "Welcome back "
						+ ChatColor.GREEN + player.getName());
				plugin.staff.add(name);
				plugin.admins.add(name);
				Bukkit.broadcastMessage(ChatColor.GOLD + "Admin "
						+ player.getName() + " has joined the game!");
				TagAPI.refreshPlayer(player);
			} else if (player.hasPermission("yupball.staff.mod")) {
				player.sendMessage(ChatColor.GOLD + "Welcome back "
						+ ChatColor.GREEN + player.getName());
				plugin.staff.add(name);
				plugin.mods.add(name);
				Bukkit.broadcastMessage(ChatColor.GOLD + "Mod "
						+ player.getName() + " has joined the game!");
				TagAPI.refreshPlayer(player);

			} else {
				player.sendMessage(ChatColor.GOLD + "Welcome back "
						+ ChatColor.GREEN + player.getName());
				TagAPI.refreshPlayer(player);
			}
		} else {
			player.sendMessage(ChatColor.GOLD + "Welcome to the server "
					+ ChatColor.GREEN + player.getName());
			Bukkit.broadcastMessage(ChatColor.GOLD + "Welcome "
					+ ChatColor.GREEN + player.getName() + ChatColor.GOLD
					+ " to the server!");
			plugin.getStats().set("Data.Stats." + player.getName() + ".Points",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Kills",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Deaths",
					0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Goals",
					0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".DeathsThisRound", 0);
			plugin.getStats().set(
					"Data.Stats." + player.getName() + ".KillsSinceDeath", 0);
			plugin.getStats().set("Data.Stats." + player.getName() + ".Rank",
					"Trainee");
			int amountPoints = plugin.getStats().getInt(
					"Data.Stats." + player.getName() + ".Points");
			player.sendMessage(plugin.message + ChatColor.GOLD
					+ "You currently have " + amountPoints + " points");
			TagAPI.refreshPlayer(player);
			plugin.saveStats();
		}
		Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(plugin, new Runnable() {
					@Override
					public void run() {
						int amountPoints = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Points");
						TabAPI.updatePlayer(player);
						String name = player.getName();
						if (player.hasPermission("yupball.staff.mod")
								|| player.hasPermission("yupball.staff.admin")
								|| player
										.hasPermission("yupball.staff.coowner")) {
							player.setDisplayName(name);
							player.setDisplayName(ChatColor.GOLD + name
									+ ChatColor.RESET);
						} else if (player.getName().equalsIgnoreCase(
								"yupie_123")) {
							player.setDisplayName(name);
							player.setDisplayName(ChatColor.DARK_PURPLE + name
									+ ChatColor.RESET);
						} else {
							player.setDisplayName(name);
							player.setDisplayName(ChatColor.WHITE + name
									+ ChatColor.RESET);
						}
						if (player.hasPermission("yupball.admin")) {
							ItemStack boots = new ItemStack(
									Material.CHAINMAIL_BOOTS, 1);
							ItemStack legs = new ItemStack(
									Material.CHAINMAIL_LEGGINGS, 1);
							ItemStack chest = new ItemStack(
									Material.CHAINMAIL_CHESTPLATE, 1);
							ItemStack head = new ItemStack(
									Material.CHAINMAIL_HELMET, 1);
							player.getInventory().setBoots(boots);
							player.getInventory().setLeggings(legs);
							player.getInventory().setChestplate(chest);
							player.getInventory().setHelmet(head);
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.GOLD + "Admin" + ChatColor.AQUA
									+ "]" + ChatColor.WHITE
									+ player.getDisplayName());
							player.getInventory().getBoots()
									.addEnchantment(Enchantment.DURABILITY, 1);
							player.getInventory().getLeggings()
									.addEnchantment(Enchantment.DURABILITY, 1);
							player.getInventory().getChestplate()
									.addEnchantment(Enchantment.DURABILITY, 1);
							player.getInventory().getHelmet()
									.addEnchantment(Enchantment.DURABILITY, 1);
							player.updateInventory();
						} else if (amountPoints < 25) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Trainee");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_GRAY + "Trainee"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 2);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.fromRGB(99, 99, 99));
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 25 && amountPoints < 100) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Amateur");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.GRAY + "Amateur"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 2);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.YELLOW);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 100 && amountPoints < 300) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Advanced");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_GREEN + "Adv"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 2);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.GREEN);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 300 && amountPoints < 1000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Forge");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_BLUE + "Forge"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 2);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.fromRGB(26, 32, 65));
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 1000 && amountPoints < 3500) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Brigadier");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_AQUA + "Brigadier"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 2);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.GREEN);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 3500 && amountPoints < 7000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Commander");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_PURPLE + "Commander"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 0);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.PURPLE);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 7000 && amountPoints < 10000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Minister");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.GOLD + "Minister"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 0);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.ORANGE);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 10000
								&& amountPoints < 25000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Prime");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.DARK_RED + "Prime"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 0);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.RED);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 25000
								&& amountPoints < 60000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"King");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.AQUA + "King" + ChatColor.AQUA
									+ "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 0);
							ItemStack LGB = new ItemStack(
									Material.LEATHER_BOOTS, 1);
							LeatherArmorMeta LGBM = (LeatherArmorMeta) LGB
									.getItemMeta();
							LGBM.setColor(Color.FUCHSIA);
							LGB.setItemMeta(LGBM);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(LGB);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 60000
								&& amountPoints < 100000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Lord");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.BLUE + "Lord" + ChatColor.AQUA
									+ "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 4);
							ItemStack boots = new ItemStack(
									Material.IRON_BOOTS, 1);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(boots);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 100000
								&& amountPoints < 150000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Legate");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.LIGHT_PURPLE + "Legate"
									+ ChatColor.AQUA + "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 4);
							ItemStack boots = new ItemStack(
									Material.GOLD_BOOTS, 1);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(boots);
							player.updateInventory();
							plugin.saveStats();
						} else if (amountPoints >= 150000) {
							player.getInventory().setArmorContents(null);
							plugin.getStats().set(
									"Data.Stats." + player.getName() + ".Rank",
									"Master");
							player.setDisplayName(ChatColor.AQUA + "["
									+ ChatColor.RED + "Master" + ChatColor.AQUA
									+ "]" + ChatColor.WHITE
									+ player.getDisplayName());
							ItemStack head = new ItemStack(Material.SKULL_ITEM,
									1, (byte) 1);
							ItemStack boots = new ItemStack(
									Material.DIAMOND_BOOTS, 1);
							player.getInventory().setHelmet(head);
							player.getInventory().setBoots(boots);
							player.updateInventory();
							plugin.saveStats();
						}

						/*
						 * if (player.hasPermission("yupball.mod")) {
						 * player.setDisplayName(ChatColor.DARK_RED + "[" +
						 * ChatColor.YELLOW + "Mod" + ChatColor.DARK_RED + "]" +
						 * player.getDisplayName()); } else
						 */if (player.isOp()) {
							player.setDisplayName(ChatColor.DARK_RED + "["
									+ ChatColor.GREEN + "Admin"
									+ ChatColor.DARK_RED + "]"
									+ player.getDisplayName());
						}/*
						 * else if (player.hasPermission("yupball.coowner")) {
						 * player.setDisplayName(ChatColor.DARK_RED + "[" +
						 * ChatColor.BLACK + "Co-" + ChatColor.YELLOW + "Own" +
						 * ChatColor.DARK_RED + "er" + ChatColor.DARK_RED + "]"
						 * + player.getDisplayName()); } else if
						 * (player.hasPermission("yupball.owner") ||
						 * player.getName().equalsIgnoreCase( "yupie_123")) {
						 * player.setDisplayName(ChatColor.DARK_RED + "[" +
						 * ChatColor.DARK_BLUE + "O" + ChatColor.DARK_GREEN +
						 * "w" + ChatColor.DARK_AQUA + "n" + ChatColor.DARK_RED
						 * + "e" + ChatColor.DARK_PURPLE + "r" +
						 * ChatColor.DARK_RED + "]" + player.getDisplayName());
						 * }
						 */
						if (plugin.getStats().contains(
								"Data.Stats." + player.getName() + ".Donator")) {
							if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("speedy")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Speedy"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("soldier")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Soldier"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("heavy")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Heavy"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("doctor")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Dr."
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("spy")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Spy"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("sniper")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Sniper"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("bomberman")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Bomber"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("supporter")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.GOLD + "Supporter"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("premium")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.GOLD + "Premium"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("precious")) {
								if (plugin.getStats().contains(
										"Data.Stats." + player.getName()
												+ ".Prefix")) {
									String prefix = plugin.getStats()
											.getString(
													"Data.Stats."
															+ player.getName()
															+ ".Prefix");
									player.setDisplayName(ChatColor.DARK_PURPLE
											+ "[" + ChatColor.DARK_AQUA
											+ prefix + ChatColor.DARK_PURPLE
											+ "]" + player.getDisplayName());
								} else {
									player.setDisplayName(ChatColor.DARK_PURPLE
											+ "[" + ChatColor.DARK_AQUA
											+ "Precious"
											+ ChatColor.DARK_PURPLE + "]"
											+ player.getDisplayName());
								}
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("ninja")) {
								player.setDisplayName(ChatColor.DARK_PURPLE
										+ "[" + ChatColor.AQUA + "Ninja"
										+ ChatColor.DARK_PURPLE + "]"
										+ player.getDisplayName());
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Donator")
									.equalsIgnoreCase("preciousplus")) {
								if (plugin.getStats().contains(
										"Data.Stats." + player.getName()
												+ ".Prefix")) {
									String prefix = plugin.getStats()
											.getString(
													"Data.Stats."
															+ player.getName()
															+ ".Prefix");
									player.setDisplayName(ChatColor.DARK_PURPLE
											+ "[" + ChatColor.DARK_AQUA
											+ prefix + ChatColor.DARK_PURPLE
											+ "]" + player.getDisplayName());
								} else {
									player.setDisplayName(ChatColor.DARK_PURPLE
											+ "[" + ChatColor.DARK_AQUA
											+ "Precious+"
											+ ChatColor.DARK_PURPLE + "]"
											+ player.getDisplayName());
								}
							} else {
								return;
							}
						}
					}

				}, 0, 40);

	}
}