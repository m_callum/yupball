package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class warnExecutor implements CommandExecutor {

	private YuPball plugin;

	public warnExecutor(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String commandLabel, String[] args) {
		if (sender.hasPermission("yupball.warn")) {
			if (args.length < 1) {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /warn <Player>");
			} else if (args.length > 1) {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /warn <Player>");
			} else {
				final Player warn = plugin.getServer().getPlayer(args[0]);
				if (warn == null) {
					sender.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "That Player is not on the server currently!");
				} else {
					if (!(plugin.getBans().contains(args[0]))) {
						plugin.getBans().set(args[0] + ".Amount", 1);
						warn.sendMessage(ChatColor.DARK_RED
								+ "You have received your first warning!");
						sender.sendMessage(plugin.message + ChatColor.GREEN
								+ args[0] + ChatColor.DARK_GRAY
								+ " has been warned!");
						plugin.saveBans();
					} else {
						if (plugin.getBans().getInt(args[0] + ".Amount") == 1) {
							plugin.getBans().set(args[0] + ".Amount", 2);
							warn.sendMessage(ChatColor.DARK_RED
									+ "You have received your second warning!");
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ args[0] + ChatColor.DARK_GRAY
									+ " has been warned!");
							plugin.saveBans();
						} else if (plugin.getBans().getInt(args[0] + ".Amount") == 2) {
							plugin.getBans().set(args[0] + ".Amount", 3);
							warn.sendMessage(ChatColor.DARK_RED
									+ "You have received your third warning!");
							Bukkit.getScheduler().scheduleSyncDelayedTask(
									plugin, new Runnable() {
										@Override
										public void run() {
											warn.kickPlayer(ChatColor.RED
													+ "You have been warned a third time!");
										}
									}, 20);
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ args[0] + ChatColor.DARK_GRAY
									+ " has been warned!");
							plugin.saveBans();
						} else if (plugin.getBans().getInt(args[0] + ".Amount") == 3) {
							plugin.getBans().set(args[0] + ".Amount", 4);
							warn.sendMessage(ChatColor.DARK_RED
									+ "You have received your fourth warning!");
							Bukkit.getScheduler().scheduleSyncDelayedTask(
									plugin, new Runnable() {
										@Override
										public void run() {
											warn.kickPlayer(ChatColor.RED
													+ "You have been warned a fourth time!");
										}
									}, 20);
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ args[0] + ChatColor.DARK_GRAY
									+ " has been warned!");
							plugin.saveBans();
						} else if (plugin.getBans().getInt(args[0] + ".Amount") == 4) {
							plugin.getBans().set(args[0] + ".Amount", 5);
							warn.sendMessage(ChatColor.DARK_RED
									+ "You have received your fifth and last warning!");
							Bukkit.getScheduler().scheduleSyncDelayedTask(
									plugin, new Runnable() {
										@Override
										public void run() {
											warn.kickPlayer(ChatColor.DARK_RED
													+ "You have been banned from this server!");
											warn.setBanned(true);
										}
									}, 20);
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ args[0] + ChatColor.DARK_GRAY
									+ " has been warned and banned!");
							plugin.saveBans();
						}
					}
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to perform this command!");
		}
		return false;
	}
}