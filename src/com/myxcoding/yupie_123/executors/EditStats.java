package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.myxcoding.yupie_123.YuPball;

public class EditStats implements CommandExecutor {

	private YuPball plugin;

	public EditStats(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender.hasPermission("yupball.editstats")) {
			if (args.length < 3) {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /editstats <Player> <Stat> <+/- Amount>");
			} else if (args.length == 3) {
				if (plugin.getStats().contains("Data.Stats." + args[0])) {
					if (args[1].equalsIgnoreCase("score")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Points")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".Points", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Points")
									+ Integer.parseInt(num);
							plugin.stats.set("Data.Stats." + args[0]
									+ ".Points", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else if (args[1].equalsIgnoreCase("kills")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Kills")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".Kills", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Kills")
									+ Integer.parseInt(num);
							plugin.stats.set(
									"Data.Stats." + args[0] + ".Kills", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else if (args[1].equalsIgnoreCase("deaths")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Deaths")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".Deaths", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Deaths")
									+ Integer.parseInt(num);
							plugin.stats.set("Data.Stats." + args[0]
									+ ".Deaths", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else if (args[1].equalsIgnoreCase("goals")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Goals")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".Goals", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".Goals")
									+ Integer.parseInt(num);
							plugin.stats.set(
									"Data.Stats." + args[0] + ".Goals", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else if (args[1].equalsIgnoreCase("won")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".MatchesWon")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".MatchesWon",
									Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".MatchesWon")
									+ Integer.parseInt(num);
							plugin.stats.set("Data.Stats." + args[0]
									+ ".MatchesWon", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else if (args[1].equalsIgnoreCase("lost")) {
						if (args[2].contains("-")) {
							String num = args[2].replace("-", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".MatchesLost")
									- Integer.parseInt(num);
							plugin.getStats().set(
									"Data.Stats." + args[0] + ".MatchesLost",
									Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						} else if (args[2].contains("+")) {
							String num = args[2].replace("+", "");
							int Final = plugin.getStats().getInt(
									"Data.Stats." + args[0] + ".MatchesLost")
									+ Integer.parseInt(num);
							plugin.stats.set("Data.Stats." + args[0]
									+ ".MatchesLost", Final);
							plugin.saveStats();
							sender.sendMessage(plugin.message + ChatColor.GREEN
									+ "You changed " + args[0] + "'s Stats!");
						}
					} else {
						sender.sendMessage(plugin.message
								+ ChatColor.DARK_RED
								+ "Available stats: Score/Lost/Won/Deaths/Kills/Goals");
					}
				} else {
					sender.sendMessage(plugin.message + ChatColor.GOLD
							+ "That player does not exist!");
				}
			} else {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /editstats <Player> <Stat> <+/-Amount>");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to use this command!");
		}
		return false;
	}

}
