package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Prefix implements CommandExecutor {

	private YuPball plugin;

	public Prefix(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.kit.precious")
					|| player.hasPermission("yupball.kit.preciousplus")) {
				if (args.length < 1 || args.length > 1) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /prefix <Prefix>");
				} else {
					plugin.getStats().set(
							"Data.Stats." + player.getName() + ".Prefix",
							args[0]);
					player.sendMessage(plugin.message + ChatColor.GREEN
							+ "You set your prefix to " + ChatColor.DARK_PURPLE
							+ "[" + ChatColor.DARK_AQUA + args[0]
							+ ChatColor.DARK_PURPLE + "]");
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
