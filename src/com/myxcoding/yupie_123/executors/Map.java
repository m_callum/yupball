package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Map implements CommandExecutor {

	private YuPball plugin;

	public Map(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.map.set")) {
				if (args.length < 2) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /map <ID> <Team1/Team2>");
				} else if (args.length == 2) {
					if (args[1].equalsIgnoreCase("team1")) {
						double X = player.getLocation().getX();
						double Y = player.getLocation().getY();
						double Z = player.getLocation().getZ();
						float Yaw = player.getLocation().getYaw();
						float Pitch = player.getLocation().getPitch();
						String world = player.getLocation().getWorld()
								.getName();
						plugin.getMaps().set("Map" + args[0] + ".Team1.World",
								world);
						plugin.getMaps().set("Map" + args[0] + ".Team1.X", X);
						plugin.getMaps().set("Map" + args[0] + ".Team1.Y", Y);
						plugin.getMaps().set("Map" + args[0] + ".Team1.Z", Z);
						plugin.getMaps().set("Map" + args[0] + ".Team1.Yaw",
								Yaw);
						plugin.getMaps().set("Map" + args[0] + ".Team1.Pitch",
								Pitch);
						plugin.saveMaps();
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You added the spawn location for Team1 on Map " + args[0]);
					} else if (args[1].equalsIgnoreCase("team2")) {
						double X = player.getLocation().getX();
						double Y = player.getLocation().getY();
						double Z = player.getLocation().getZ();
						float Yaw = player.getLocation().getYaw();
						float Pitch = player.getLocation().getPitch();
						String world = player.getLocation().getWorld()
								.getName();
						plugin.getMaps().set("Map" + args[0] + ".Team2.World",
								world);
						plugin.getMaps().set("Map" + args[0] + ".Team2.X", X);
						plugin.getMaps().set("Map" + args[0] + ".Team2.Y", Y);
						plugin.getMaps().set("Map" + args[0] + ".Team2.Z", Z);
						plugin.getMaps().set("Map" + args[0] + ".Team2.Yaw",
								Yaw);
						plugin.getMaps().set("Map" + args[0] + ".Team2.Pitch",
								Pitch);
						plugin.saveMaps();
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You added the spawn location for Team2 on Map " + args[0]);
					} else if (args[1].equalsIgnoreCase("spectate")) {
						double X = player.getLocation().getX();
						double Y = player.getLocation().getY();
						double Z = player.getLocation().getZ();
						float Yaw = player.getLocation().getYaw();
						float Pitch = player.getLocation().getPitch();
						String world = player.getLocation().getWorld()
								.getName();
						plugin.getMaps().set("Map" + args[0] + ".Spectate.World",
								world);
						plugin.getMaps().set("Map" + args[0] + ".Spectate.X", X);
						plugin.getMaps().set("Map" + args[0] + ".Spectate.Y", Y);
						plugin.getMaps().set("Map" + args[0] + ".Spectate.Z", Z);
						plugin.getMaps().set("Map" + args[0] + ".Spectate.Yaw",
								Yaw);
						plugin.getMaps().set("Map" + args[0] + ".Spectate.Pitch",
								Pitch);
						plugin.saveMaps();
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You added the spectate location on Map " + args[0]);
					}

				}
			} else if (player.hasPermission("yupball.map.view")) {
				if (plugin.Arena1.contains(player)) {
					String map = plugin.getMaps().getString(
							"Map" + plugin.map1 + ".Team1.World");
					player.sendMessage(plugin.message + ChatColor.GREEN
							+ "Next map: " + map);
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to do this!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED + "");
		}
		return false;
	}
}
