package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Kit implements CommandExecutor {

	private YuPball plugin;

	public Kit(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 1 || args.length < 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /kit <Rank>");
			} else if (args.length == 1) {
				try {
					if (args[0].equalsIgnoreCase("speedy")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("speedy")) {
							plugin.Speedy(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Speedy Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("soldier")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("soldier")) {
							plugin.Soldier(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Soldier Kit!");
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Soldier Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("heavy")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("heavy")) {
							plugin.Heavy(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Heavy Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("doctor")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("doctor")) {
							plugin.Doctor(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Doctor Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("spy")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("spy")) {
							plugin.Spy(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Spy Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("sniper")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("sniper")) {
							plugin.Sniper(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Sniper Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("bomberman")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("bomberman")) {
							plugin.Bomberman(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Bomberman Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else if (args[0].equalsIgnoreCase("ninja")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + player.getName()
												+ ".Donator")
								.equalsIgnoreCase("ninja")) {
							plugin.Ninja(player);
							player.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were given the Ninja Kit!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have permission to perform this command!");
						}
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "Please use a valid rank name!");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}
}
