package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class ChatChannel implements CommandExecutor {

	private YuPball plugin;

	public ChatChannel(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (p.hasPermission("yupball.chat")) {
				if (args.length < 1 || args.length > 1) {
					p.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage /ch <Channel Name>");
				} else {
					if (args[0].equalsIgnoreCase("staff")) {
						if (p.hasPermission("yupball.chat.staff")) {
							plugin.NormalChat.remove(p);
							plugin.DonatorChat.remove(p);
							plugin.StaffChat.add(p);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were moved to the Staff Chat!");
						} else {
							p.sendMessage(plugin.message + ChatColor.DARK_RED
									+ "You do not have permission to do this!");
						}
					} else if (args[0].equalsIgnoreCase("donate")) {
						if (plugin
								.getStats()
								.getString(
										"Data.Stats." + p.getName()
												+ ".Donator")
								.equalsIgnoreCase("speedy")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("soldier")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("heavy")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("doctor")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("spy")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("sniper")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("bomberman")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("ninja")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("supporter")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("premium")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("precious")
								|| plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("preciousplus")) {
							plugin.NormalChat.remove(p);
							plugin.StaffChat.remove(p);
							plugin.DonatorChat.add(p);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were moved to the Donator Chat!");
						} else {
							p.sendMessage(plugin.message + ChatColor.DARK_RED
									+ "You do not have permission to do this!");
						}
					} else if (args[0].equalsIgnoreCase("normal")) {
						if (p.hasPermission("yupball.chat.normal")) {
							plugin.DonatorChat.remove(p);
							plugin.StaffChat.remove(p);
							plugin.NormalChat.add(p);
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "You were moved to the Normal Chat!");
						} else {
							p.sendMessage(plugin.message + ChatColor.DARK_RED
									+ "You do not have permission to do this!");
						}
					} else {
						p.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "Please provide a correct channel name!");
					}
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}
}
