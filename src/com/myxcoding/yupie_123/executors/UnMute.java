package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class UnMute implements CommandExecutor {

	private YuPball plugin;

	public UnMute(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2,
			String[] args) {
		if (sender.hasPermission("yupball.unmute")) {
			if (args.length == 1) {
				Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
				if (targetPlayer != null) {
					plugin.getStats().set(
							"Data.Stats." + targetPlayer.getName() + ".Muted",
							false);
					sender.sendMessage(plugin.message + ChatColor.GREEN
							+ "You unmuted " + args[0]);
					targetPlayer.sendMessage(plugin.message + ChatColor.YELLOW
							+ "You have been unmuted");
				} else {
					sender.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "That player is not online!");
				}
			} else {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /unmute <Player>");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to do this!");
		}
		return false;
	}

}
