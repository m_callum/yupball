package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class CommandCmd implements CommandExecutor {

	private YuPball plugin;

	public CommandCmd(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender,
			org.bukkit.command.Command command, String label, String[] args) {
		if (sender.hasPermission("yupball.command")) {
			if (args.length > 2 || args.length > 2) {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /command <Player> <Command>");
				Player targetPlayer = plugin.getServer().getPlayer(args[0]);
				if (targetPlayer != null) {
					Bukkit.dispatchCommand(targetPlayer, args[1]);
				}
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to do this!");
		}
		return false;
	}

}
