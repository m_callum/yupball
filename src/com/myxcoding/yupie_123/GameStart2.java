package com.myxcoding.yupie_123;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class GameStart2 extends BukkitRunnable {

	private YuPball plugin;
	public int number = 3;

	public GameStart2(YuPball plugin) {
		this.plugin = plugin;
	}

	public static int map1 = 0;
	public static int mapAmount;

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(plugin, new Runnable() {
					@Override
					public void run() {
						mapAmount = plugin.getConfig().getInt("NumberofMaps");
						Random randomGenerator = new Random();
						int randomInt = randomGenerator.nextInt(mapAmount);
						for (Player p : plugin.Arena2) {
							p.sendMessage(plugin.message + ChatColor.GREEN
									+ "Game Started!");
							if (randomInt == plugin.getConfig().getInt(
									"Arena 1 MapID")) {
								randomInt = randomGenerator.nextInt(mapAmount);
							}
							plugin.getConfig().set("Arena 2 MapID", randomInt);
							plugin.saveConfig();
							plugin.InGame1 = true;
							map1 = randomInt;
							for (Player s : plugin.purpleTeam) {
								String world = plugin.getMaps().getString(
										"Map" + map1 + ".Team1.World");
								double X = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team1.X");
								double Y = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team1.Y");
								double Z = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team1.Z");
								float Yaw = plugin.getMaps().getInt(
										"Map" + map1 + ".Team1.Yaw");
								float Pitch = plugin.getMaps().getInt(
										"Map" + map1 + ".Team1.Pitch");
								Bukkit.getServer().createWorld(
										new WorldCreator(world));
								Location team1 = new Location(plugin
										.getServer().getWorld(world), X, Y, Z,
										Yaw, Pitch);
								s.teleport(team1);
								s.setBedSpawnLocation(team1);
								if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("trainee")) {
									plugin.Trainee(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("amateur")) {
									plugin.Amateur(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Advanced")) {
									plugin.Advanced(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Forge")) {
									plugin.Forge(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Brigadier")) {
									plugin.Brigadier(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Commander")) {
									plugin.Commander(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Minister")) {
									plugin.Minister(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Prime")) {
									plugin.Prime(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("King")) {
									plugin.King(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Lord")) {
									plugin.Lord(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Legate")) {
									plugin.Legate(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Master")) {
									plugin.Master(s);
								}
								if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("speedy")) {
									plugin.Speedy(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("soldier")) {
									plugin.Soldier(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("heavy")) {
									plugin.Heavy(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -2);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("doctor")) {
									plugin.Doctor(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -1);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("spy")) {
									plugin.Spy(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("sniper")) {
									plugin.Sniper(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("bomberman")) {
									plugin.Bomberman(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("ninja")) {
									plugin.Ninja(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("supporter")) {
									plugin.Soldier(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 5));
									s.updateInventory();
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("premium")) {
									plugin.Soldier(s);
									plugin.Speedy(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -2);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 10));
									s.updateInventory();
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("precious")) {
									plugin.Speedy(s);
									plugin.Soldier(s);
									plugin.Spy(s);
									plugin.Sniper(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 15));
									s.updateInventory();
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Donator")
										.equalsIgnoreCase("preciousplus")) {
									plugin.Speedy(s);
									plugin.Soldier(s);
									plugin.Spy(s);
									plugin.Sniper(s);
									plugin.Heavy(s);
									plugin.Ninja(s);
									plugin.Bomberman(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 15));
									s.updateInventory();
								}
							}
							for (Player s : plugin.goldTeam) {
								String world = plugin.getMaps().getString(
										"Map" + map1 + ".Team2.World");
								double X = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team2.X");
								double Y = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team2.Y");
								double Z = plugin.getMaps().getDouble(
										"Map" + map1 + ".Team2.Z");
								float Yaw = plugin.getMaps().getInt(
										"Map" + map1 + ".Team2.Yaw");
								float Pitch = plugin.getMaps().getInt(
										"Map" + map1 + ".Team2.Pitch");
								Bukkit.getServer().createWorld(
										new WorldCreator(world));
								Location Team2 = new Location(plugin
										.getServer().getWorld(world), X, Y, Z,
										Yaw, Pitch);
								s.teleport(Team2);
								s.setBedSpawnLocation(Team2);
								if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("trainee")) {
									plugin.Trainee(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("amateur")) {
									plugin.Amateur(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Advanced")) {
									plugin.Advanced(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Forge")) {
									plugin.Forge(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Brigadier")) {
									plugin.Brigadier(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Commander")) {
									plugin.Commander(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Minister")) {
									plugin.Minister(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Prime")) {
									plugin.Prime(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("King")) {
									plugin.King(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Lord")) {
									plugin.Lord(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Legate")) {
									plugin.Legate(s);
								} else if (plugin
										.getStats()
										.getString(
												"Data.Stats." + p.getName()
														+ ".Rank")
										.equalsIgnoreCase("Master")) {
									plugin.Master(s);
								}
								if (p.hasPermission("yupball.kit.speedy")) {
									plugin.Speedy(s);
								} else if (p
										.hasPermission("yupball.kit.soldier")) {
									plugin.Soldier(s);
								} else if (p.hasPermission("yupball.kit.heavy")) {
									plugin.Heavy(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -2);
								} else if (p
										.hasPermission("yupball.kit.Doctor")) {
									plugin.Doctor(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -1);
								} else if (p.hasPermission("yupball.kit.spy")) {
									plugin.Spy(s);
								} else if (p
										.hasPermission("yupball.kit.sniper")) {
									plugin.Sniper(s);
								} else if (p
										.hasPermission("yupball.kit.bomberman")) {
									plugin.Bomberman(s);
								} else if (p.hasPermission("yupball.kit.ninja")) {
									plugin.Ninja(s);
								} else if (p
										.hasPermission("yupball.kit.supporter")) {
									plugin.Soldier(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 5));
									s.updateInventory();
								} else if (p
										.hasPermission("yupball.kit.premium")) {
									plugin.Soldier(s);
									plugin.Speedy(s);
									plugin.getStats().set(
											"Data.Stats." + s.getName()
													+ ".DeathsThisRound", -2);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 10));
									s.updateInventory();
								} else if (p
										.hasPermission("yupball.kit.precious")) {
									plugin.Speedy(s);
									plugin.Soldier(s);
									plugin.Spy(s);
									plugin.Sniper(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 15));
									s.updateInventory();
								} else if (p
										.hasPermission("yupball.kit.precious+")) {
									plugin.Speedy(s);
									plugin.Soldier(s);
									plugin.Spy(s);
									plugin.Sniper(s);
									plugin.Heavy(s);
									plugin.Ninja(s);
									plugin.Bomberman(s);
									s.getInventory().addItem(
											new ItemStack(Material.STICK, 15));
									s.updateInventory();
								}
							}
						}
					}
				}, 20L);
	}
}
