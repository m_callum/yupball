package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Score implements CommandExecutor {

	private YuPball plugin;

	public Score(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (args.length == 0) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
					if (plugin.getConfig().contains(
							"Data.Stats." + player.getName())) {
						plugin.stats.set(player.getName() + ".Points", 0);
						String playerName = player.getName();
						int amountPoints = plugin.getConfig().getInt(
								"Data.Stats." + playerName + ".Points");
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have " + ChatColor.GREEN + amountPoints
								+ ChatColor.GOLD + " points");
					} else {
						plugin.stats.set(player.getName() + ".Points", 0);
						String playerName = player.getName();
						plugin.getConfig().set(
								"Data.Stats." + playerName + ".Points", 0);
						plugin.getConfig().set(
								"Data.Stats." + player.getName() + ".Kills", 0);
						plugin.getConfig()
								.set("Data.Stats." + player.getName()
										+ ".Deaths", 0);
						plugin.getConfig().set(
								"Data.Stats." + player.getName() + ".Goals", 0);
						plugin.getConfig().set(
								"Data.Stats." + player.getName()
										+ ".DeathspluginRound", 0);
						int amountPoints = plugin.getConfig().getInt(
								"Data.Stats." + playerName + ".Points");
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have " + ChatColor.GREEN + amountPoints
								+ ChatColor.GOLD + " points");
						plugin.saveConfig();
					}
			} else {
				sender.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You need to be ingame to perform that command!");
			}
		} else if (args.length == 1) {
				if (plugin.getConfig().contains("Data.Stats." + args[0])) {
					String playerName = args[0];
					int amountPoints = plugin.getConfig().getInt(
							"Data.Stats." + playerName + ".Points");
					sender.sendMessage(plugin.message + ChatColor.GOLD
							+ args[0] + " currently has " + amountPoints
							+ " points");
				} else {
					sender.sendMessage(plugin.message + ChatColor.GOLD
							+ "That player does not exist!");
				}
		} else if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Usage: /score [Player]");
		}
		return false;
	}
}
