package com.myxcoding.yupie_123.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlace implements Listener {

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Block b = event.getBlockPlaced();
		if (!event.getPlayer().hasPermission("yupball.map.edit")) {
			event.setCancelled(true);
		}
		if (b.getType() == Material.STONE_PLATE) {
			event.setCancelled(false);
		} else {
			if (b.getType().equals(Material.STRING)) {
				event.setCancelled(true);
			}
			if (b.getType().equals(Material.FURNACE)) {
				b.setTypeId(62);
			}
		}
	}

}
