package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class SetSpawn implements CommandExecutor {

	private YuPball plugin;

	public SetSpawn(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.setspawn")) {
				double spawnX = player.getLocation().getX();
				double spawnY = player.getLocation().getY();
				double spawnZ = player.getLocation().getZ();
				String world = player.getWorld().getName();
				float yaw = player.getLocation().getYaw();
				float pitch = player.getLocation().getPitch();
				plugin.getConfig().set("Spawn.X", spawnX);
				plugin.getConfig().set("Spawn.Y", spawnY);
				plugin.getConfig().set("Spawn.Z", spawnZ);
				plugin.getConfig().set("Spawn.World", world);
				plugin.getConfig().set("Spawn.Yaw", yaw);
				plugin.getConfig().set("Spawn.Pitch", pitch);
				plugin.saveConfig();
				player.sendMessage(plugin.message + ChatColor.GOLD
						+ "You set the worlds spawn location!");
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to perform this command!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
