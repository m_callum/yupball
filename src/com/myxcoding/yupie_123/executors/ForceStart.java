package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import com.myxcoding.yupie_123.GameStart1;
import com.myxcoding.yupie_123.GameStart2;
import com.myxcoding.yupie_123.YuPball;

public class ForceStart implements CommandExecutor {

	private YuPball plugin;
	public int number;

	public ForceStart(YuPball plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean onCommand(CommandSender player, Command command,
			String label, String[] args) {
		if (player.hasPermission("yupball.forcestart")) {
			if (args.length > 1 || args.length < 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /forcestart <1/2>");
			} else {
				if (args[0].equalsIgnoreCase("1")) {
					if (plugin.InGame1 == false) {
						plugin.Goals1red = 0;
						plugin.Goals1blue = 0;
						for(Player p : plugin.Arena1) {
							p.getInventory().clear();
							p.setGameMode(GameMode.SURVIVAL);
						}
						BukkitTask GameStart1 = new GameStart1(plugin)
								.runTaskTimer(plugin, 0, -1);
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You started the game in Arena 1!");
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That game is in progress already!");
					}
				} else if (args[0].equalsIgnoreCase("2")) {
					if (plugin.InGame2 == false) {
						plugin.Goals2gold = 0;
						plugin.Goals2purple = 0;
						for(Player p : plugin.Arena2) {
							p.getInventory().clear();
							p.setGameMode(GameMode.SURVIVAL);
						}
						BukkitTask GameStart2 = new GameStart2(plugin)
								.runTaskTimer(plugin, 0, 40);
						player.sendMessage(plugin.message + ChatColor.GREEN
								+ "You started the game in Arena 2!");
					} else {
						player.sendMessage(plugin.message + ChatColor.DARK_RED
								+ "That game is in progress already!");
					}
				}
			}
		} else {
			player.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You do not have permission to do this!");
		}
		return false;
	}
}
