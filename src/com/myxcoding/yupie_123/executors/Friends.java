package com.myxcoding.yupie_123.executors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Friends implements CommandExecutor {

	private YuPball plugin;

	public Friends(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.friends")) {
				if (args.length < 2 || args.length > 2) {
					player.sendMessage(plugin.message + ChatColor.DARK_RED
							+ "Usage: /friends <Add/Remove> <Player Name>");
				} else {
					if (args[0].equalsIgnoreCase("add")) {
						if (plugin.getStats().getInt(
								"Data.Stats." + player.getName()
										+ ".Friends.Amount") != 3) {
							if (plugin.getStats().contains(
									"Data.Stats." + player.getName()
											+ ".Friends.Amount")) {
								int Amount = (int) plugin.getStats().get(
										"Data.Stats." + player.getName()
												+ ".Friends.Amount") + 1;
								int Before = Amount - 1;

								plugin.getStats().set(
										"Data.Stats." + player.getName()
												+ ".Friends.Amount", Amount);
								Player targetPlayer = Bukkit.getServer()
										.getPlayer(args[1]);
								if (plugin
										.getStats()
										.getString(
												"Data.Stats."
														+ player.getName()
														+ ".Friends." + Before)
										.equalsIgnoreCase(args[1])) {
									player.sendMessage(plugin.message
											+ ChatColor.DARK_RED
											+ "That player is already one of your friends!");
								} else if (plugin.getStats().contains(
										"Data.Stats." + player.getName()
												+ ".Friends." + Before
												+ args[1])) {

								} else {
									if (targetPlayer != null) {
										plugin.getStats().set(
												"Data.Stats."
														+ player.getName()
														+ ".Friends." + Amount,
												args[1]);
										player.sendMessage(plugin.message
												+ ChatColor.GREEN
												+ "You added " + args[1]
												+ " to your friends list!");
										plugin.saveStats();
									} else {
										player.sendMessage(plugin.message
												+ ChatColor.DARK_RED
												+ "The player you are trying to add needs to be online!");
									}
								}
							} else {
								int Amount = 1;
								plugin.getStats().set(
										"Data.Stats." + player.getName()
												+ ".Friends.Amount", Amount);
								Player targetPlayer = Bukkit.getServer()
										.getPlayer(args[1]);
								if (targetPlayer != null) {
									plugin.getStats().set(
											"Data.Stats." + player.getName()
													+ ".Friends." + Amount,
											args[1]);
									player.sendMessage(plugin.message
											+ ChatColor.GREEN + "You added "
											+ args[1]
											+ " to your friends list!");
									plugin.saveStats();
								} else {
									player.sendMessage(plugin.message
											+ ChatColor.DARK_RED
											+ "The player you are trying to add needs to be online!");
								}
							}
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You have exceeded the friend limit!");
						}
					} else if (args[0].equalsIgnoreCase("remove")) {
						if (plugin.getStats().getInt(
								"Data.Stats." + player.getName()
										+ ".Friends.Amount") == 0) {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "You do not have any friends!");
						} else {
							int Amount = plugin.getStats().getInt(
									"Data.Stats." + player.getName()
											+ ".Friends.Amount");
							if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Friends.1")
									.equalsIgnoreCase(args[1])) {
								plugin.getStats().set(
										"Data.Stats." + player.getName()
												+ ".Friends.1", null);
								plugin.getStats()
										.set("Data.Stats." + player.getName()
												+ ".Friends.Amount", Amount - 1);
								player.sendMessage(plugin.message
										+ ChatColor.GREEN + "You removed "
										+ args[1] + " from your friends list!");
								plugin.saveStats();
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Friends.2")
									.equalsIgnoreCase(args[1])) {
								plugin.getStats().set(
										"Data.Stats." + player.getName()
												+ ".Friends.2", null);
								plugin.getStats()
										.set("Data.Stats." + player.getName()
												+ ".Friends.Amount", Amount - 1);
								player.sendMessage(plugin.message
										+ ChatColor.GREEN + "You removed "
										+ args[1] + " from your friends list!");
								plugin.saveStats();
							} else if (plugin
									.getStats()
									.getString(
											"Data.Stats." + player.getName()
													+ ".Friends.3")
									.equalsIgnoreCase(args[1])) {
								plugin.getStats().set(
										"Data.Stats." + player.getName()
												+ ".Friends.3", null);
								plugin.getStats()
										.set("Data.Stats." + player.getName()
												+ ".Friends.Amount", Amount - 1);
								player.sendMessage(plugin.message
										+ ChatColor.GREEN + "You removed "
										+ args[1] + " from your friends list!");
								plugin.saveStats();
							} else {
								player.sendMessage(plugin.message
										+ ChatColor.DARK_RED
										+ "That player is not on your friends list!");
							}
						}
					}
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to do this!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}
}
