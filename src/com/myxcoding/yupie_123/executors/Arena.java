package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Arena implements CommandExecutor {

	private YuPball plugin;

	public Arena(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 1) {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "Usage: /arena <join/leave> <1/2>");
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("join")) {
					if (args[1].equalsIgnoreCase("1")) {
						plugin.Arena1.add(player);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You are now part of Arena 1");
					} else if (args[1].equalsIgnoreCase("2")) {
						plugin.Arena2.add(player);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You are now part of Arena 2");
					}
				} else if (args[0].equalsIgnoreCase("leave")) {
					if (args[1].equalsIgnoreCase("1")) {
						plugin.Arena1.remove(player);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been removed from Arena 1!");
					} else if (args[1].equalsIgnoreCase("2")) {
						plugin.Arena2.remove(player);
						player.sendMessage(plugin.message + ChatColor.GOLD
								+ "You have been removed from Arena 2!");
					}
				} else if (args[0].equalsIgnoreCase("setlobby")) {
					if (!player.hasPermission("yupball.arena.setlobby")) {
						player.sendMessage(plugin.message
								+ ChatColor.DARK_RED
								+ "You don't have permission to use that command!");
					} else {
						if (args[1].equalsIgnoreCase("1")
								|| args[1].equalsIgnoreCase("2")) {
							Double X = player.getLocation().getX();
							Double Y = player.getLocation().getY();
							Double Z = player.getLocation().getZ();
							float Yaw = player.getLocation().getYaw();
							float Pitch = player.getLocation().getPitch();
							String world = player.getWorld().getName();
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.X", X);
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.Y", Y);
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.Z", Z);
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.World",
									world);
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.Yaw",
									Yaw);
							plugin.getConfig().set(
									"Arena.Arena " + args[1] + ".Lobby.Pitch",
									Pitch);
							player.sendMessage(plugin.message
									+ ChatColor.GOLD
									+ "You have set the lobby location for Arena "
									+ args[1] + "!");
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "Usage: /arena setlobby <1/2>");
						}
					}
				}
			}
		} else {
			sender.sendMessage(plugin.message
					+ ChatColor.DARK_RED
					+ "You need to be an ingame player to perform that command!");
		}
		return false;
	}

}