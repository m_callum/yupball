package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class FireballCmd implements CommandExecutor {

	private YuPball plugin;

	public FireballCmd(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.fireball")) {
				player.launchProjectile(Fireball.class);
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to perform this command!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}

}
