package com.myxcoding.yupie_123.executors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.myxcoding.yupie_123.YuPball;

public class Stats implements CommandExecutor {

	private YuPball plugin;

	public Stats(YuPball plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("yupball.stats")) {
				if (args.length == 0) {
					if (plugin.getStats().contains(
							"Data.Stats." + player.getName())) {
						int playerKills = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Kills");
						int playerDeaths = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Deaths");
						int playerGoals = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Goals");
						player.sendMessage(ChatColor.GOLD + "----------"
								+ ChatColor.AQUA + "Stats:" + ChatColor.GOLD
								+ "----------");
						player.sendMessage(ChatColor.GREEN + "Kills: "
								+ ChatColor.GOLD + playerKills);
						player.sendMessage(ChatColor.RED + "Deaths: "
								+ ChatColor.GOLD + playerDeaths);
						player.sendMessage(ChatColor.BLUE + "Goals: "
								+ ChatColor.GOLD + playerGoals);
						player.sendMessage(ChatColor.AQUA + "Date Last Seen: "
								+ ChatColor.GOLD + player.getLastPlayed());
					} else {
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Kills", 0);
						plugin.getStats()
								.set("Data.Stats." + player.getName()
										+ ".Deaths", 0);
						plugin.getStats().set(
								"Data.Stats." + player.getName() + ".Goals", 0);
						plugin.saveStats();
						int playerKills = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Kills");
						int playerDeaths = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Deaths");
						int playerGoals = plugin.getStats().getInt(
								"Data.Stats." + player.getName() + ".Goals");
						player.sendMessage(ChatColor.GOLD + "----------"
								+ ChatColor.AQUA + "Stats:" + ChatColor.GOLD
								+ "----------");
						player.sendMessage(ChatColor.GREEN + "Kills: "
								+ ChatColor.GOLD + playerKills);
						player.sendMessage(ChatColor.RED + "Deaths: "
								+ ChatColor.GOLD + playerDeaths);
						player.sendMessage(ChatColor.BLUE + "Goals: "
								+ ChatColor.GOLD + playerGoals);
						player.sendMessage(ChatColor.AQUA + "Date Last Seen: "
								+ ChatColor.GOLD + player.getLastPlayed());
					}
				} else if (args.length == 1) {
					if (player.hasPermission("yupball.stats.others")) {
						Player targetPlayer = sender.getServer().getPlayer(
								args[0]);
						if (plugin.getStats().contains(
								"Data.Stats." + targetPlayer.getName())) {
							int playerKills = plugin.getStats().getInt(
									"Data.Stats." + targetPlayer.getName()
											+ ".Kills");
							int playerDeaths = plugin.getStats().getInt(
									"Data.Stats." + targetPlayer.getName()
											+ ".Deaths");
							int playerGoals = plugin.getStats().getInt(
									"Data.Stats." + targetPlayer.getName()
											+ ".Goals");
							player.sendMessage(ChatColor.GOLD + "----------"
									+ ChatColor.AQUA + "Stats:"
									+ ChatColor.GOLD + "----------");
							player.sendMessage(ChatColor.GREEN + "Kills: "
									+ ChatColor.GOLD + playerKills);
							player.sendMessage(ChatColor.RED + "Deaths: "
									+ ChatColor.GOLD + playerDeaths);
							player.sendMessage(ChatColor.BLUE + "Goals: "
									+ ChatColor.GOLD + playerGoals);
							player.sendMessage(ChatColor.AQUA
									+ "Date Last Seen:" + ChatColor.GOLD
									+ player.getLastPlayed());
						} else {
							player.sendMessage(plugin.message
									+ ChatColor.DARK_RED
									+ "That player does not exist");
						}
					} else {
						player.sendMessage(plugin.message
								+ ChatColor.DARK_RED
								+ "You do not have permission to perform this command!");
					}
				}
			} else {
				player.sendMessage(plugin.message + ChatColor.DARK_RED
						+ "You do not have permission to perform this command!");
			}
		} else {
			sender.sendMessage(plugin.message + ChatColor.DARK_RED
					+ "You need to be an ingame player to do this!");
		}
		return false;
	}
}
